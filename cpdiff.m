function [tau, slope] = cpdiff( r, t, varargin )

psth = getArg( varargin, 'psth', [] );
dt = t(2)-t(1);
dr = diff(r)/dt;
ddr = diff(dr)/dt;
[pks, locs] = findpeaks( abs(ddr) );

% [~,I] = max(abs(ddr));

if isempty(locs)
    tau = 0;
    slope = 0;
    return
end

I = locs(1);
slope = mean(dr(locs(1):end));
tau = t(locs(1));
ss = inf;

for i = 2:length(locs)
    y = @(t) (t >= tau).*slope.*(t - tau) + r(locs(i));
    css = sum( (y(t) - r).^2 );
    
    if css < ss
        ss = css;
        I = locs(i);        
    end
    
    tau = t(locs(i));
    slope = mean(dr(locs(i):end));   
end

slope = mean(dr(I:end));
tau = t(I);
