function [r, params] = barsfilt( r, varargin )
% BARSFILT  Performs a BARS filtering

addpath( 'bars' )

draw = getArg( varargin, 'Draw', false );
t = getArg( varargin, 'Time', 1:length(r) );
r = getArg( varargin, 'OriginalRate', r );
interval = getArg( varargin, 'Interval', [] );
interval = getArg( varargin, 'Cut', interval );

res = barsP( r, [-1,0], 50 );
params = struct( 'OriginalRate', r );
r_filt = res.mean';

if draw
    plot( t, r )
    hold on
    plot( t, r_filt, 'r' )        
    hold off
    title( 'Trends extracted by BARS' )
    pause
end

r = r_filt;