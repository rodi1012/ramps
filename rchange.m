function [r, params] = rchange( r, varargin )
% rateChange   Computes the change point following the algorithm:

draw = getArg( varargin, 'Draw', false );
t = getArg( varargin, 'Time', 1:length(r) ); 
method = getArg( varargin, 'method', 'confidence' );
p = 0.7;
D = 20;
delta = @(k) min( k + D, length(r) );
% r = r - mean(r);



switch method    
    case 'ttest'
        p_values = ones( 1, length(t) );
        % Detection of a change point 
        % TO_DO: log likelihood
        
        for k_est = D:length(t)
            mu_est = mean( r(1:k_est) );
            mu2 = mean( r( k_est+1:delta(k_est) ) );
            std_est = std( r(1:k_est) );
            std2 = std( r( k_est+1:delta(k_est) ) );
            v = D;    
            tv = (mu2 - mu_est)/(std2/sqrt(v));
            p_values(k_est) = 1-tcdf( abs(tv), v-1 );
        end
        
        kc = find( p_values < .001, 1, 'first' );

    case 'confidence'
        k_est = .2*length(t);
        mu_est = mean( r(1:k_est) );
        std_est = std( r(1:k_est) );
        theta = 2*std_est;
        P = @(k, theta) sum( abs(r(k+1:delta(k))-mu_est) > theta )/D;
        pconds = arrayfun( @(k)P(k, theta) > p, 1:length(t) );
        kc = find( pconds, 1, 'first' );
        
        if isempty( kc )
            kc = 1;
        end
end

[tau, slope] = refine( r, t, kc, 'lines', false );
params = struct( 'Tau', tau, 'Slope', slope );

if draw
    plot( t, r, 'k' );
    hold on
    plot( t(kc), r(kc), 'r.', 'markersize', 15 )    
    hold off
    title('Change point by rate change (press a key to continue)')
    pause
end



