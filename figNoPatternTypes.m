function figNoPatternTypes( h, varargin )
% Plots the heatmaps of the firing rates sorted by starting point


figure(h)
clf(h)
region = getArg( varargin, 'Region', [] );
X_ramp = getArg( varargin, 'X_ramp', [] );
taus = getArg( varargin, 'Taus', [] );
alphas = getArg( varargin, 'Alphas', [] );
numChangePoints = getArg( varargin, 'NumberChangePoints', [] );
t = getArg( varargin, 'Time', [] );
type = getArg( varargin, 'Type', [] );
map = getArg( varargin, 'Colormap', jet );
interval = getArg( varargin, 'Interval', [] );

t = t{1};
I_data = find( arrayfun(@(i)strcmp( type{i}, 'no_pattern' ), 1:length(type)) );

function plotRate( I_cp, tau_cp, alpha_cp )
    if isempty( I_cp )
        return
    end
    
    Xp = X_ramp( I_cp, : );  
%      for i = 1:size(Xp,1)
%         plot( t, Xp(i,:) )
%         axis( [interval(1) interval(2) -3 3 ] )
%         pause
%     end
    
    plot( t, mean( Xp, 1 ), 'k' )  
    axis( [interval(1) interval(2) -3 3 ] )
    xlabel( 'Time (ms)' ) 
end

function plotSub( I_cp, tau_cp, alpha_cp )
    if isempty( I_cp )
        return
    end
    
    X = X_ramp( I_cp, : );
       imagesc( 'XData', t, 'YData', 1:length(I_cp), 'CData', X )
    axis([interval(1) interval(2) 1 length(I_cp)])
    colorbar
    colormap(map)
end

subplot(2,1,1)
% 1 Changing point
plotSub( I_data, [], [] );
title( 'No evidence of patterns' )

subplot (2,1,2)
plotRate( I_data, [], [] );


end