function [tau, slope] = refine( r, t, kc, varargin )
% refine    Fits a line for the slope and two lines for a refinement of the
% change point

lines = getArg( varargin, 'lines', true );

% Linear Fitting
X = [ones(length(t)-kc+1, 1), t(kc:end)'];
Y = r( kc:end )';
B = X\Y;

if lines    
    % Estimation of the base line
    d = @(p) abs( -B(2)*p(1) + p(2) - B(1) )/sqrt( B(2)^2 + 1);
    ldist = zeros( 1, kc );

    for i = 1:kc
        ldist(i) = d( [t(i), r(i)] );
    end

    kl = find( ldist > 0.2, 1, 'last' );

    % Second line
    X = [ones(kl, 1), t(1:kl)'];
    Y = r( 1:kl )';
    B0 = X\Y;

    tau = -(B0(1) - B(1))/(B0(2) - B(2));
else
    tau = t(kc);
end

slope = B(2);

