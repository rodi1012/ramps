function [alpha, taus, beta, LL] = fitpw( r, t, theta0, varargin )
    
%     fp = @(t) max(0, t);
%     L = @(t, alpha, tau) alpha'*fp( t - tau );
    identity = getArg( varargin, 'Identity', false );
    interval = getArg( varargin, 'Interval', [-1, 0]);
    F = @(t) max(t,0);    

    function s = fun( theta )
        n = length(theta); % n is expected to be even
        I = n/2;
        alpha = theta( 1:I );
        taus = [interval(1) theta( I+1:end-1 ) interval(2)];
        beta = theta(end);
        H = @(t) min([t(ones(1,n/2)); taus(2:end)]);
        L = @(t) alpha*F(H(t) - taus(1:end-1))' + beta;
        LL = @(t) arrayfun( @(i)L(t(i)), 1:length(t));
        
        s = sum( (r - LL(t)).^2 );
    end
    
    n = length(theta0);
    if identity
        theta = theta0;
    else
        opts = optimset( 'Display', 'iter' );
        LB = [-inf*ones(1,n/2) t(1)*ones(1, n/2-1) -inf];
        UB = [inf*ones(1,n/2) t(end)*ones(1, n/2-1) inf];
        theta = fmincon( @fun, theta0, [], [], [], [], LB, UB );
    end
    
    n = length(theta);
    I = n/2;
    alpha = theta( 1:I );
    taus = [interval(1) theta( I+1:end-1 ) interval(2)];
    beta = theta(end);
    H = @(t) min([t(ones(1,n/2)); taus(2:end)]);
    L = @(t) alpha*F(H(t) - taus(1:end-1))' + beta;
    LL = @(t) arrayfun( @(i)L(t(i)), 1:length(t));
end