%% Example poisson regression
close all

n = 50;
x = linspace( 0, 1, n );
b = [0.1 2];
lambda = @(x) exp(b(1) + b(2)*x);
y = zeros(1,n);

for i = 1:n
    y(i) = poissrnd( lambda(x(i)) );
end

plot(x,y)

% Possion regression

L = @(theta) -sum( y.*(theta(1) + theta(2)*x) - exp(theta(1) + theta(2)*x));
theta0 = randi(20,2,1);

opts = optimset('Display','iter');
theta = fminsearch( L, theta0, opts );

theta
hold on
plot( t, exp(theta(1) + theta(2)*x), 'r--' )
hold off






%% Test the ramp fit
close all
clc
m = 100;
t = linspace( 0, 1, m );
tau = 0.7;
b0 = 3;
b1 = 20;
lambda = @(b0, b1, tau, t) b0 + (t > tau).*(b1*(t - tau));
% plotting
y = arrayfun( @(k)poissrnd( lambda(b0, b1, tau, t(k)) ), 1:length(t) );
result = rampfit( y, t )

figure
clf
plot(t, y )
hold on
plot( t, lambda(b0, b1, tau, t), 'r--' )
% plot( t, exp(lambda(result.B0, result.B1, result.tau, t)), 'r' )
plot( t, lambda(result.B0, result.B1, result.tau, t), 'r' )
hold off

