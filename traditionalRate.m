function [r, t] = traditionalRate( x, w, s, I )
% x - array with the trial data for the neurons
% w - window width
% s - step size
% I - interva;

t = I(1):s:I(2);
r = zeros( 1, length(t) );
m = max( x(:,1) );

for k = 1:length(t)
    r(k) = sum( abs(x(:,2) - t(k)) <= w/2 )/(w*m);
end