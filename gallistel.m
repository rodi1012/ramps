function [tau, slope] = gallistel( r, t, varargin )

addpath('gallistel')
type = getArg( varargin, 'type', 'normal' );

switch type
    case 'normal'
        cp = cp_wrapper( r', 1, 3, 4 );
    case 'binomial'
        cp = cp_wrapper( r', 1, 1, 3 );        
end
    
cp = cp( find( cp(:,1) > 0, 1, 'first' ), 1 );
[tau, slope] = refine( r, t, cp, varargin{:} );


end