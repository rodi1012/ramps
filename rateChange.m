function [tau, slope] = rateChange( r, t )
% rateChange   Computes the change point following the algorithm:
% 1. Estimate base line
% 2. The first point above or below 2SD and stable for K muestras is the
% first candidate
% 3. 

t_est = linspace( t(1), t(end), 100 );

m_base = zeros( size(t_est) );
s_base = zeros( size(t_est) );

for i = 1:length(t_est)
	m_base(i) = mean( r( t <= t_est(i) ) );
    s_base(i) = sqrt(var( r( t <= t_est(i) ) ));
end

subplot 121
plot( t_est, m_base, 'r' )
hold on
plot( t, r, 'k' )
hold off
xlabel('Time of estimation')
ylabel('Mean')
subplot 122
plot( t_est, s_base, 'r'  ) 
hold on
plot( t, r, 'k'  )
hold off
xlabel('Time of estimation')
ylabel('Sigma')

tau = 0;
slope = 0;
