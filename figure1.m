function figure1( h, varargin )
% Plots the heatmaps of the firing rates sorted by starting point

figure(h)
clf(h)
region = getArg( varargin, 'Region', [] );
X_ramp = getArg( varargin, 'X_ramp', [] );
slopes = getArg( varargin, 'Slopes', [] );
taus = getArg( varargin, 'Taus', [] );
taus_o = getArg( varargin, 'OriginalTaus', [] );
t = getArg( varargin, 'Time', [] );
map = getArg( varargin, 'Colormap', hot );
interval = getArg( varargin, 'Interval', [] );
sortby = getArg( varargin, 'SortBy', 'tau' );
order = getArg( varargin, 'Order', 0 ); % 0 is the normal function, 1 the derivative

Xp = X_ramp( slopes >= 0, : );
Xn = X_ramp( slopes < 0, : );
taup = taus( slopes >= 0 );
taun = taus( slopes < 0 );

if ~isempty(taus_o)
    taup_o = taus_o( slopes >= 0 );
    taun_o = taus_o( slopes < 0 );
end
 
switch sortby
    case 'tau'        
        [~,I] = sort(taup);
    case 'slope'
        [~,I] = sort(slopes( slopes >= 0 ));        
end

switch order
    case 0
        f = @(X) X;
        LIMS = [0,1];
    case 1
        f = @(X)estimateDerivative( X, t );
        LIMS = [-1,1];
end

subplot 121 
imagesc( 'XData', t, 'YData', 1:size(Xp,1), 'CData', f(Xp(I,:)), LIMS )
colorbar
colormap( map )
hold on
plot( taup(I), 1:size(Xp,1), '.-', 'markersize', 10, 'color', [0,0,0] ); 

if ~isempty(taus_o)
    plot( taup_o(I), 1:size(Xp,1), '.', 'markersize', 10, 'color', [0,1,0] ); 
end

hold off
axis([interval(1) interval(2) 1 max([2, size(Xp,1)])])
title(sprintf('Positive slope (%s)', region))
xlabel('time before cue')

switch sortby
    case 'tau'        
        [~,I] = sort(taun);
    case 'slope'
        [~,I] = sort(slopes( slopes < 0 ));        
end

subplot 122
imagesc( 'XData', t, 'YData', 1:size(Xn,1), 'CData', f(Xn(I,:)), LIMS )
colorbar
hold on
plot( taun(I), 1:size(Xn,1), '.-', 'markersize', 5, 'color', [0,0,0] );

if ~isempty(taus_o)
    plot( taun_o(I), 1:size(Xn,1), '.', 'markersize', 10, 'color', [0,1,0] ); 
end

hold off
axis([interval(1) interval(2) 1 max([2, size(Xn,1)])])
title(sprintf('Negative slope (%s)', region))
xlabel('time before cue')
end

function dX = estimateDerivative( X, t )
    % Improve the estimation
    dX = zeros( size(X) );
    addpath('bars')
    
    for i = 1:size(X,1)
        r = X(i,: );
%         p = pchip( t, r, t );
        bp = barsP( abs(r), [ -1, 0], 1 );
        p = bp.mean';
%         plot(  t, p )
%         hold on
%         plot( t, r, 'r--' )
%         hold off
%         pause
        dp = p(2:end) - p(1:end-1);
        dX(i, :) = [dp, dp(end)];
    end
    
end