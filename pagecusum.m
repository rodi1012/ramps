function [r, params] = pagecusum( r, varargin )
% Implements Page's original cusum
% 
% Koepcke, L., Ashida, G., & Kretzberg, J. (2016). 
% Single and Multiple Change Point Detection in Spike Trains: 
% Comparison of Different CUSUM Methods. Frontiers in systems neuroscience, 10.

t = getArg( varargin, 'Time', 1:length(r) ); 
draw = getArg( varargin, 'Draw', true );
distribution = getArg( varargin, 'distribution', 'empirical' );
% Assuming normalized variables
h = getArg( varargin, 'threshold', 6 );
delta = getArg( varargin, 'delta', 2 );

%Change assumed additive
switch distribution
    case 'poisson'
        s = @(j, mu0, sigma, delta) r(j)*log((delta + mu0)/mu0) - delta;
    case 'gamma'
        error( 'Not supported' )
    case 'normal'
        s = @(j, mu0, sigma, delta) (delta/(sigma^2)).*(r(j) - mu0 - delta/2);
    case 'empirical'
        error( 'Not supported' )
end

stop = false;
k = 2;
S = zeros( 2, length(r)+1 ); % cumulative sum
G = zeros( 2, length(r)+1 ); % Decision function
nc = [];

while ~stop && k < length(r)
    % Estimates mu0 and sigma    
    mu0 = mean( r(1:k-1) );
    sigma = std( r(1:k-1) );
    
    if sigma > 0
        sk = [s(k, mu0, sigma, delta); s(k, mu0, sigma, -delta)];
        S(:,k) = S(:,k-1) + sk;
        G(:,k) = [max(G(1, k-1) + sk(1), 0); max(G(2, k-1) + sk(2), 0)];

        if sum(G(:, k) > h) == 1 || k >= length(r)
            nd = k;
            [~, nc] = min( S( G(:, k) > h, 2:k ) );
            stop = true;
        end
    end
    k = k + 1;
end

if isempty(nc)
    nc = length(r);
end

tau = t(nc);
slope = (r(end) - r(nc))/(t(end)-t(nc));
params = struct( 'Tau', tau, 'Slope', slope );


if draw
    clf
    subplot 131
    plot( t, r )
    hold on

    plot( [t(nc) t(nc)], [-max(abs(r)) max(abs(r))], 'r--' );
    hold off

    subplot 132

    plot( t, S(1, 2:end), 'b' )
    hold on
    plot( t, S(2, 2:end), 'r' )
    hold off

    subplot 133
    plot( t, G(1, 2:end), 'b' );
    hold on
    plot( t, G(2, 2:end), 'r' );
    plot( [t(1) t(end)], [h, h], 'k--' )
    hold off
    pause
end
