function [sr, params] = sqroottx( r, varargin )
% SQROOTTX applies the square root transformation to the data

draw = getArg( varargin, 'Draw', false );
t = getArg( varargin, 'Time', false );
sr = sqrt(r);

if draw
    subplot 121
    plot( t, r )
    subplot 122
    plot( t, sr, 'r' );
    hold off
%     axis( [-1 0 -10 10] )
    pause
end


params = struct;