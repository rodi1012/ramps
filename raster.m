function raster( X )
% Raster plot of trials

plot( [X(:,2), X(:,2)]', [X(:,1)-.5, X(:,1)+.5]', 'k', 'linewidth', 2.0 )
xlabel( 'time' )
ylabel( 'trial' )

