function figure2( h, varargin )
% Plots the histograms of taus and slopes
figure(h)
clf(h)

region = getArg( varargin, 'Region', [] );
X_ramp = getArg( varargin, 'X_ramp', [] );
slopes = getArg( varargin, 'Slopes', [] );
taus = getArg( varargin, 'Taus', [] );
t = getArg( varargin, 'Time', [] );
map = getArg( varargin, 'Colormap', hot );

Xp = X_ramp( slopes >= 0, : );
Xn = X_ramp( slopes < 0, : );
taup = taus( slopes >= 0 );
taun = taus( slopes < 0 );

subplot 121
[h1, b] = histcounts( taup, linspace( -1, 0, 40 ) );
[h2, b] = histcounts( taun, b );
%     histogram( taus, b, 'FaceColor', [.5 .5 .5] )
hb = bar( (b(1:end-1) + b(2:end))/2, h1' );
hold on
hb.FaceColor = [0.7 0.3 0.3];
hb = bar( (b(1:end-1) + b(2:end))/2, -h2' );
hb.FaceColor = [0.3 0.3 0.7];
xlabel('time before cue')
legend('Positive', 'Negative')
hold off
title(sprintf('Changing point (%s)', region))
subplot 122
slopes( isnan(slopes) ) = [];
nl = length(slopes);
slopes_sorted = sort(slopes);
nl = length(slopes_sorted);
Q1 = slopes_sorted(fix(nl*0.25));
Q3 = slopes_sorted(fix(nl*0.75));

k = 1.5;
ind_low = find( slopes_sorted > Q1 - k*(Q3 - Q1), 1, 'first' );
ind_high = find( slopes_sorted < Q1 + k*(Q3 - Q1), 1, 'last' );

if isempty( ind_low )
    ind_low = 1;
end

if isempty( ind_high )
    ind_high = nl;
end

 slopes_sorted( ind_low )
 slopes_sorted( ind_high )
histogram( slopes, linspace( slopes_sorted( ind_low ), slopes_sorted( ind_high ), 50) )
xlabel('time before cue')
title(sprintf('Slope (%s)', region))