function [X, params] = fano( X, varargin )
% FANO  Computes the fano factor of the spike counts

draw = getArg( varargin, 'Draw', false );

T = max( X(:,1) );
c_trials = zeros( 1, T );

for i = 1:size(X,1)    
    c_trials( X( i, 1 ) ) = c_trials( X( i, 1 ) ) + 1;
end

ff = var( c_trials )/mean( c_trials );
params = struct( 'Fano', ff );

if draw
    clf
    histogram( c_trials, 30 );
    pause
end



