function respool = doAnalysis( Xspikes, varargin )

rate = getArg( varargin, 'Rate', [] );
detection = getArg( varargin, 'Detection', [] );
n_bins = getArg( varargin, 'NBins', 100 );
interval = getArg( varargin, 'Interval', [-1, 0] );
dzscore = getArg( varargin, 'ZScore', false );
dsmooth = getArg( varargin, 'Smooth', false );
dpreselect = getArg( varargin, 'PreSelect', true );
dcut = getArg( varargin, 'Cut', [] );

if dpreselect
    [Xrate, t] = rmatrix( Xspikes, 'Interval', interval );        
    Xrate = cell2mat( ...
            arrayfun( @(k)normncut( Xrate(k,:), t ), ...
            1:size(Xrate,1), 'UniformOutput', false )');        
    [Xsel, idxs] = prefilter( Xrate, t, 'Interval', interval, 'method', 'pca' );
    Xspikes = Xspikes(idxs);
end

respool = struct( 'Xramp', [], ...
                 'taus', [], ...
                 'slopes', [], ...
                 'ps', [], ...
                 't', []);

for k = 1:length(Xspikes)
    trials = Xspikes{k};    
    [r,t] = rate( trials );
    ro = r;
    to = t;
    
    % moving average
    M = 20;
    tp = linspace(interval(1), interval(2), M);
    tm = (tp(2:end) + tp(1:end-1))/2;
    rp = zeros(1,M-1);
    
    for l = 1:M-1
        I = find( t >= tp(l) & t < tp(l+1) );
        rp(l) = mean( r(I) );
    end
    
    % Cuting in the desited interval
    if ~isempty(dcut)
        fprintf('Cutting\n')
        [rp,tm] = normncut( rp, tm );
        [r,t] = normncut( r, t );
    end

    if dsmooth
        fprintf('Smoothing by BARS\n')
        try
            res = barsP( r, [-1,0], max(trials(:,1)) );
            ro = r;
            r = res.mean';
        catch
            continue;
        end
    end

    if dzscore 
        fprintf('ZScoring\n')
        rp = zscore( rp, 'base', trials );            
    end

    result = detection( rp, tm, r, t );       
    
    figure(1)
    clf
    I = find( to < 0 );
    to = to(I);
    ro = ro(I);

%     subplot 131
%     plot( to, ro )
%     hold on
%     plot( [result.tau1, result.tau1], [min(ro) max(ro)], 'r--' )
%     hold off
%     subplot 132
%     hist( ro(1:find( to <= result.tau1, 1, 'last')), 20 )
%     subplot 133
%     hist( ro(1:find( to > result.tau1, 1, 'last')), 20 )
    subplot 121
    plot( ro )
    hold on
    plot(r)
    subplot 122
    N = length(ro);
    R = zeros( N, N );

for i = 1:N
    for j = 1:N        
        R(i,j) = abs(ro(i) - ro(j)) < 0.001;
    end
end

imagesc(R)

    pause
    % pooling the results
    respool = sadd( respool, ...
                   'Xramp', r, ...
                   'taus', result.tau1, ...
                   'slopes', result.slope, ...
                   'ps', result.p);
end

respool.t = t;