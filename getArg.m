function v = getArg( args, name, defValue )
% getArg    Extracts an argument from a list of key,value pairs

N = length(args);

if mod( length(args), 2 )  
    error( 'Incorrect format!' )
end

v = defValue;

for i = 1:2:N
    if strcmp( args{i}, name )
        v = args{i+1};
    end
end
