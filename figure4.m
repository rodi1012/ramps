function figure4( h, varargin )
% Heat maps with neurons opf 4 
% 1. Good fir
% 2. Evidence of change
% 3. Evidence of ramp
% 4. Evidence of variance change


figure(h)
clf(h)

X = getArg( varargin, 'X_ramp', [] );
slopes = getArg( varargin, 'Slopes', [] );
taus = getArg( varargin, 'Taus', [] );
taus_o = getArg( varargin, 'OriginalTaus', [] );
t = getArg( varargin, 'Time', [] );
R2 = getArg( varargin, 'R2', [] );
direction = getArg( varargin, 'Direction', 'positive' );
pchange = getArg( varargin, 'PChange', [] );
pramp = getArg( varargin, 'PRamp', [] );
pvar = getArg( varargin, 'PVar', [] );
order = getArg( varargin, 'Order', 0 ); % 0 is the normal function, 1 the derivative
map = getArg( varargin, 'Colormap', hot );
interval = getArg( varargin, 'Interval', [] );
region = getArg( varargin, 'Region', [] );

switch order
    case 0
        f = @(X) X;
        LIMS = [0,1];
    case 1
        f = @(X)estimateDerivative( X, t );
        LIMS = [-1,1];
end


function plotGroup( X, ctaus, ctaus_o )
    I = sort( ctaus );
    imagesc( 'XData', t, 'YData', 1:size(X,1), 'CData', X, LIMS )
    colorbar
    colormap( map )
    hold on
    plot( ctaus, 1:size(X,1), '.-', 'markersize', 5, 'color', [0,0,0] );

    if ~isempty(taus_o)
        plot( ctaus_o, 1:size(X,1), '.', 'markersize', 10, 'color', [0,1,0] ); 
    end
    axis([interval(1) interval(2) 1 max([2, size(X,1)])])

end


subplot 221
% Evidence of goodfit
I_R2 = find( R2 > 0.1 );
X_R2 = X(I_R2,:);
tau_R2 = taus(I_R2);
slopes_R2 = slopes(I_R2);

if strcmp( direction, 'positive' )
    plotGroup( X_R2(slopes_R2>0,:), tau_R2(slopes_R2>0), [] );
else
    plotGroup( X_R2(slopes_R2<0,:), tau_R2(slopes_R2<0), [] );
end
title(sprintf('Good fit (%s)', region))

subplot 222
% Evidence of change
% ALERT: this is very prone to errors
I_pchange = find( pchange < 0.05 );
X_pchange = X(I_pchange,:);
tau_pchange = taus(I_pchange);
slopes_pchange = slopes(I_pchange);

if strcmp( direction, 'positive' )
    plotGroup( X_pchange(slopes_pchange>0,:), tau_pchange(slopes_pchange>0), [] );
else
    plotGroup( X_pchange(slopes_pchange<0,:), tau_pchange(slopes_pchange<0), [] );
end

title(sprintf('Evidence of change (%s)', region))

subplot 223
% Evidence of ramp
I_pramp = find( pramp < 0.05 );
X_pramp = X(I_pramp,:);
tau_pramp = taus(I_pramp);
slopes_pramp = slopes(I_pramp);

if strcmp( direction, 'positive' )
    plotGroup( X_pramp(slopes_pramp>0,:), tau_pramp(slopes_pramp>0), [] );
else
    plotGroup( X_pramp(slopes_pramp<0,:), tau_pramp(slopes_pramp<0), [] );
end

title(sprintf('Evidence of ramp (%s)', region))

subplot 224
% Evidence of variance
I_pvar = find( pvar < 0.05 );
X_pvar = X(I_pvar,:);
tau_pvar = taus(I_pvar);
slopes_pvar = slopes(I_pvar);

if strcmp( direction, 'positive' )
    plotGroup( X_pvar(slopes_pvar>0,:), tau_pvar(slopes_pvar>0), [] );
else
    plotGroup( X_pvar(slopes_pvar<0,:), tau_pvar(slopes_pvar<0), [] );
end

title(sprintf('Change in variance (%s)', region))

end


