function [X, t] = rmatrix( neurons, varargin )
% Computes the Standarized Matrix of rates

interval = getArg( varargin, 'Interval', [] );
doz = getArg( varargin, 'ZScore', false ); 

w = 0.050;
s = 0.010; % 0.001
nsmooth = 5; % NUmber of samples
X = [];

for i = 1:length(neurons)   
    [r, t] = tradrate( neurons{i}, ...
                       'WindowWidth', w, ...
                       'StepSize', s, ...
                       'Interval', interval );

    if doz
        r = zscore( r, 'base', neurons );
    end
    % Smooth   
    r = smooth( r, nsmooth );
    X = [X;r]; 
end
