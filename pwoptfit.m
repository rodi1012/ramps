function [r, params] = pwoptfit( r, varargin )
% PWOPTFIT detects the best fitting ramp model following the algorithm:
% 1. Find trend
% 2. Simplify
% 3. Fit
% 4. Assess likelihood of the data + p-values
% 5. Reduce dimensionality
% 6. Choose the best model

t = getArg( varargin, 'Time', 1:length(r) );
[r_trend, params] = bilateralfilt( r, varargin{:} );
[r_trend, params] = simplify( r_trend, varargin{:} );

plist = params.Simplification;
taus = plist( :, 1 );
a = (plist(2:end, 2) - plist(1:end-1,2))./(plist(2:end, 1) - plist(1:end-1,1));
b = plist( 1, 2 );

function f = lambda( taus, a, b, x )
     
end

function f = ilambda( taus, a, b  )

end

function ll = L( ctaus, ca, cb ) 
    irate = ilambda( ctaus, ca, cb ) ;   
    ll = sum( r_trend.*log( irate ) - irate );
end

function ll = g( theta )
    if mod( length(theta), 2 ) ~= 0
        error('Incorrect number of parameters')
    end
    
    % 2n parameters
    n = length(theta)/2;
    ctaus = theta( 1:n-1 );
    ca = theta( n:2*n-1 );
    cb = theta( 2*n );
    
    % sum of squares
    ll = sum( (lambda( ctaus, ca, cb, t ) - r_trend ).^2 );
%     log likelhood
%     ll = -L( ctaus, ca, cb );
end

opts = optimset('Display','on');
aic = zeros(1,size(plist,1)-1);
theta0 = [taus(2:end-1)', a', b];

for k = 1:size(plist,1)-1    
    theta = fminsearch( @g, theta0, opts );

    n = length(theta)/2;
    ctaus = theta( 1:n-1 );
    ca = theta( n:2*n-1 );
    cb = theta( 2*n );

    % Next we compute the likelihood of the data based on the 
    likelihood = L( ctaus, ca, cb );
    % (Make parametric bootstrap)
    % Compute AIC
    aic(k) = 2*likelihood;
    % Now we remove the point with less distance 
    h = zeros( 1, n-1 );
    points = [ plist(1,1), ctaus, plist(end,1) ];
    f = lambda( ctaus, ca, cb, points );

    for i = 2:n
        lpre = sqrt((f(i) - f(i-1))^2 + (points(i)-points(i-1))^2);
        l0 = sqrt((f(i+1) - f(i-1))^2 + (points(i+1)-points(i-1))^2);
        lpos = sqrt((f(i+1) - f(i))^2 +(points(i+1)-points(i))^2);
        s = (lpre + l0 + lpos)/2;
        h(i-1) = 2*sqrt(s*abs(s - l0)*abs(s - lpre)*abs(s - lpos))/l0;
    end

    % Removing the minimum
    [~, I] = min(h);
    
    ctaus(I) = [];
    pI = I+1;
    ca(I) = [];
    ca(I) = (f(pI+1) - f(pI-1))/(points(pI+1)-points(pI-1));
    theta0 = [ctaus, ca, cb];
    
    plot( t, r, 'k' );
    hold on
    plot( t, r_trend, 'k--' )
    plot( t, lambda( ctaus, ca, cb, t ), 'r' )
    hold off
    pause
end

subplot 121
aic
plot(aic)
subplot 122
plot( t, r, 'k' );
hold on
plot( t, r_trend, 'k--' )
plot( t, lambda( ctaus, ca, cb, t ), 'r' )
hold off
pause
end



