function [s, params] = kdvfilt( r, varargin )
% Applies the kdv filter 

interval = getArg( varargin, 'Interval', [] );
time = getArg( varargin, 'Time', 1:length(r));

M = 128;
t = linspace(interval(1), interval(2), M);
s = spline( time, r, t );
psi = linspace( 0, 1, 10 );

for jj = 1:length(psi)
    % x = linspace(-1,1,N);
    
    delta_x = t(2) - t(1);
    delta_k = 2*pi/(M*0.5);
    k = [0:delta_k:M/2*delta_k,-(M/2-1)*delta_k:delta_k:-delta_k];
    
    f = exp(-s.^2);
    
    f_hat = fft(f);
    u = real(ifft(f_hat.*exp(1i*k.^3*psi(jj))));
    subplot 131
    plot( time, r )    
    subplot 132
    plot( t, s )
    subplot 133
    plot(t, u)
    title( sprintf( 'time = %.2f', psi(jj) ) )
    pause
end

params = struct;