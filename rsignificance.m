function p = rsignificance( r, t, tau, varargin )
% Significance for the slope - showing evidence of the trend

method = getArg( varargin, 'Method', 'tratio' );

% Linear Fitting
kc = find( t <= tau, 1, 'last' );

if isempty(kc)
    p = 1;
    return
end

X = [ones(length(t)-kc+1, 1), t(kc:end)'];
Y = r( kc:end )';
B = X\Y;
L = @(t) B(1) + B(2)*t ;

switch method
    case 'tratio'
        % Standard error
        n = length(t) - kc + 1;
        s2 = sum( (r(kc:end) - L(t(kc:end))).^2 )/(n - 2);

        mt = mean( t(kc:end) );

        SE = sqrt(s2/sum((t(kc:end) - mt).^2));
        tratio = abs(B(2)/SE);
        p = 1 - tcdf( tratio, n-2 );

    case 'permutation' 
%         tc = t(kc:end);
%         rc = r( kc:end );
%         rm = mean( rc );
%         rp = L(tc);
%         SSE = sum( (rc - rp).^2 );
%         SST = sum( (rc - rm).^2 );
%         R2 = 1 - SSE/SST;        
%         
%         
%         X = [ones(length(t)-kc+1, 1), tp'];
%         Y = L';
%         B = X\Y;
% 
%         L = @(t) B(1) + B(2)*t;
% 
%         % Standard error
%         n = length(t) - kc + 1;
%         s2 = sum( (r(kc:end) - L(t(kc:end))).^2 )/(n - 2);
% 
%         mt = mean( t(kc:end) );
% 
%         SE = sqrt(s2/sum((t(kc:end) - mt).^2));
%         tratio = abs(B(2)/SE);
%         p = 1 - tcdf( tratio, n-2 );
        error('Not supported')
end


