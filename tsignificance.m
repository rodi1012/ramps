function [r, params] = tsignificance( r, varargin )
% Computing:
% - Goodness of fit
% - Evidence of change
% - Evidence of ramp
% - Evidence of change of standard deviation

t = getArg( varargin, 'Time', 1:length(r) );
tau = getArg( varargin, 'Tau', 1 );
interval = getArg( varargin, 'Interval', [] );

% Evidence of change
% In the evidence of change H0: mu1 = mu2
% Poisson assumption
n = length(r);
LLR = @(x, l1, l0) sum( x )*log( l1/l0 ) - n*(l1 - l0);
I_cp = find( t <= tau, 1, 'last' );

if isempty( I_cp )   
    I_cp = n;
end

x = r( I_cp+1:end );
l0 = mean( r(1:I_cp) );
l1 = mean( x );
p_change = 1 - chi2cdf( -2*LLR(x, l1, l0), n );

% Evidence of ramp
X = [ones(length(t)-I_cp, 1), t(I_cp+1:end)'];
Y = x';
B = X\Y;
L = @(t) B(1) + B(2)*t ;
t_x = t(I_cp+1:end);
s2 = sum( (x - L(t_x)).^2 )/(n - I_cp - 1);
mt = mean( t_x );
SE = sqrt( s2/sum( (t_x - mt).^2));

tratio = abs(B(2)/SE);
p_ramp = 1 - tcdf( tratio, n - I_cp -1 );

% Evidence of change in variance
% Mejor likelihood ratio test

if (n - I_cp) > 10 && I_cp > 10 
    x1 = r(1:I_cp);
    x2 = r(I_cp+1:end);
    [~,p_variance] = vartest2(x1,x2);
else
    p_variance = 1;
end

params = struct( 'PChange', p_change, ...
                 'PRamp', p_ramp, ...
                 'PVar', p_variance );