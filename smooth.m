function r = smooth( r, w )
% smooth    Applies a filter to the data
% w width of the window in samples

r = filtfilt( ones(1,w)/w, 1, r );