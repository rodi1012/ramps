function [r, params] = simplify( r, varargin )
% SIMPLIFY    Simplifies the curve as line sergments
    
    minPoints = 5;
    draw = getArg( varargin, 'Draw', false );
    epsilon = getArg( varargin, 'SimpEpsilon', 0.1 );
    t = getArg( varargin, 'Time', 1:length(r) );
    p = [ t', r' ];
    plist = doSimplify( p, epsilon, r ) ;
    
    while size(plist,1) < minPoints
        epsilon = epsilon - 0.01;
        plist = doSimplify( p, epsilon, r );
    end
   
    
    if draw
        plot( t, r )
        hold on
        plot( plist(:,1), plist(:,2), 'r' )
        hold off
%         axis( [-1 0 0 1] )
        title('Simplification of the curve')
        pause
    end
    
    params = struct( 'Simplification',  plist );    
end

function plist = doSimplify( p, epsilon, r )
    m = size(p,1);
    
    if m == 2
        plist = p;
        return
    end
    
    p1 = p(1,:);
    p2 = p(end,:);
    rest = 2:m-1;
    dists = arrayfun( @(i) pdist( p( i, :), p1, p2 ), 1:m );
    [d, I] = max( dists(rest) );
    I = rest(I);
    
    if d > epsilon 
        plist1 = doSimplify( p( 1:I, : ), epsilon, r ); 
        plist2 = doSimplify( p( I+1:end, : ), epsilon, r );        
        plist = [plist1; plist2];
    else
        plist = [p1; p2];
    end
    
end

function d = pdist( p, p1, p2 )
%     u = (p2 - p1);
%     
%     if norm(u) == 0
%         d = 0;
%         return
%     end
%     
%     u = u/norm(u);
%     up = (u*p')*u;
%     d = norm(p - up);
    x1 = p1(1);
    x2 = p2(1);
    y1 = p1(2);
    y2 = p2(2);
    d = abs( (y2 - y1)*p(1) - (x2 - x1)*p(2) + x2*y1 - y2*x1 )/norm(p2-p1);
end
