function [r, params] = findramps( r, varargin )
% FINDRAMPS   Finds the ramps by:
% 1. Bilateral filtering
% 2. Doing regression
% 3. Finding peaks distance map greater than p*FR
% 4. Fitting piecewise
% 5. Repeating in each sub interval
% ---
% 6. Computing likelihood
% 7. Decrease threshold?
%
% - Goodness of fit
% - Parametric bootstrap

m = length(r);
t = getArg( varargin, 'Time', 1:m );
interval = getArg( varargin, 'Interval', [-1, 0] );
% First
ro = r;
[r, params] = bilateralfilt( r, 'Time', t );

if any(isnan(r))
    params = struct;
    return
end

p = 2*std( ro - r )/sqrt(m);

% Second
X = [ones(size(r))', t' ];
Y = r';
B = X\Y;
% Distance map
d = @(p) abs( p(2) - B(2)*p(1) - B(1) )/sqrt(1 + B(2)^2);
sd = arrayfun( @(i) d([t(i), r(i)]), 1:length(t) ) ;
[pks, locs] = findpeaks( sd );
locs = locs( pks > p );
% Fit

if all(locs ~= 1)
    locs = [1, locs];
end

if all(locs ~= m)
    locs = [locs, m];
end

% plot( t, r );
% hold on
% plot( t(locs), r(locs), 'r*' )
% hold off
% pause

alpha = (r(locs(2:end)) - r(locs(1:end-1)))./(t(locs(2:end)) - t(locs(1:end-1)));
locs( locs == 1 ) = [];
locs( locs == m ) = [];
taus = t(locs);

theta0 = [alpha, taus, r(1)];
% The fit can be on the simplified or on the original curve
% theta = fitpw( r, t, theta0 );
% Here we are supposed to find the maximum likelihood estimate
[alpha, taus, beta, LL] = fitpw( r, t, theta0 );
rate = LL(t);
% Implementin likelihood 
% sg = std( ro - rate );
sg = 1;
lilivec = @(rate)arrayfun(@(i)normpdf(ro(i), rate(i), sg), 1:m);
lili = @(rate) log(prod( lilivec(rate) ));
% lili = @(rate) sum( ro.*log(rate*dt) - rate*dt );
gof = @(rate)sum( (ro - rate).^2./rate );
sst = sum( (ro - mean(ro)).^2 );
sse = @(rate) sum( (ro - rate).^2 );
R2 = @(rate) 1 - sse(rate)/sst;

l1 = lili(rate);
gof0 = gof(rate);
R20 = R2(rate);

% Now we try to exclude neurons which don't have any apparent ramp/pattern
% using bootstrap
% The null distribution
mu0 = mean( ro );
s0 = sg;

G = 1000;
c = zeros(1, G);

for i = 1:G
   U = randn(1,length(r))*s0 + mu0;
   % Ideally: use likelihood functions
   lilivecp = @(rate)arrayfun(@(i)normpdf(U(i), rate(i), sg), 1:m);
   lilip = @(rate) log(prod( lilivecp(rate) ));
   l0 = lilip(rate);
   c(i) = -(l0 - l1);
    
%    gofp = @(rate)sum( (U - rate).^2./rate );
%    c = c + (gof0>gofp(rate));
end

% figure(2)
% histogram( c, 40 )
% pause
p_pattern = sum(c(c<3))/G;

params = struct;

if p_pattern > 0.05
    params.Type = 'no_pattern';
    fprintf('No evidence of pattern!\n')
    
%     plot( t, ro, 'color', [0 .7 0] )
%     hold on
%     plot( t, r, 'r' )
%     axis([interval(1) 0 -3 3 ] )
%     hold off
%     pause
%     
    
    params.NumberChangePoints = -1;
else
    % This assumes that there is a ramp somewhere
    % trimming - Goodness of fit/ Likelihood
    % Deleting point by point and cheking the goodness of fit or likelihood
    
    if R20 < 0.4 
        params.Type = 'weak_pattern';       
        fprintf('Weak pattern!\n')
    else
        params.Type = 'full_pattern';
    end
       
    remove = inf;
    iterations = 0;
%     
%     figure(1)
%         clf
%         plot( t, r, 'b' )
%         hold on
%         plot( t, ro, 'color', [0.7 0.7 0.7] );
%         plot( t, LL(t), 'r' );    
%         plot( [taus; taus], repmat([min(r); max(r)], 1, length(taus)), 'r--' )
%         axis([interval(1) 0 -3 3 ] )
%         goftest = 1-chi2cdf(gof0,1);
%         title(sprintf('Original fit\nL=%.2f, GOF=%.2f, R2=%.2f', l1, goftest, R20))
%         hold off
%         pause
%         
    while ~isempty(remove)
    
    remove = [];
        
    for i = 2:length(taus)-1
        
        if (taus(i)-taus(i-1)) < 0.01
            remove = [remove, i];
            continue;
        end
        
        ntaus = taus( [1:i-1 i+1:end] );
        alpha_c = (LL(taus(i+1))-LL(taus(i-1)))./(taus(i+1) - taus(i-1));
        nalpha = [alpha(1:i-2)  alpha_c  alpha( i+1:end )] ;
        nbeta = beta;
        n = length(nalpha)*2;    

        F = @(t) max(t,0);
        H_p = @(t) min([t(ones(1,n/2)); ntaus(2:end)]);
        L_p = @(t) nalpha*F(H_p(t) - ntaus(1:end-1))' + nbeta;
        LL_p = @(t) arrayfun( @(i)L_p(t(i)), 1:length(t));

        lrtest = 1-chi2cdf(-2*(lili(LL_p(t))-lili(rate)), m);
        goftest = 1-chi2cdf(gof(LL_p(t)),1);
        % R2 can improve with the removal of points
        % That's why no a5bsolute value is put
        R2test = (R2(rate) - R2(LL_p(t))) < 0.1;
        
%         LL_p(interval(end))
%         
%         figure(1)
%         clf
%         plot( t, r, 'b' )
%         hold on
%         plot( t, ro, 'color', [0.7 0.7 0.7] );
%         plot( taus, LL(taus), 'r*' )
%         plot( t, LL(t), 'r--' );
%         plot( t, LL_p(t), 'r' );
%         title(sprintf('Removing points\nLR=%.2f, GOF=%.2f (%.2f), R2=%.2f (%.2f)', ...
%                        lrtest, goftest, gof(rate), R2test, R2(rate)))
%         hold off
%         pause
        
        if R2test
           % Marc to remove
           remove = [remove, i];
        end 
        
    end
    % Remove
    if ~isempty( remove )
        taus(remove) = [];
        alpha_t = [LL(taus)];
        alpha = (alpha_t(2:end) - alpha_t(1:end-1))./(taus(2:end) - taus(1:end-1));
        theta0 = [alpha, taus(2:end-1), beta];
        [alpha, taus, beta, LL] = fitpw( r, t, theta0, 'Identity', false );        
        rate = LL(t);
    end
    
    iterations = iterations + 1;
    
    if iterations > 10
        error( 'It seems to be an infinite cicle' )
    end
    end
    
    if R2(rate) < 0.4
        params.Type = 'weak_pattern';       
        fprintf('Weak pattern!\n')
    end
    
    params.NumberChangePoints = length(taus)-2;
    params.Taus = taus;
    params.Alphas = alpha;
    params.Beta = beta;
%     
%     figure(1)
% clf
% plot( t, r, 'b' )
% hold on
% plot( t, ro, 'color', [0.8 0.8 0.8] );
% plot( t, LL(t), 'r' );
% plot( [taus; taus], repmat([min(ro); max(ro)], 1, length(taus) ), 'r--' )
% axis( [interval(1) 0 -3 3 ] );
% title(sprintf('Final RESULT'))
% hold off
% pause
end

