function [pidxs, nidxs, pc, nc] = preselect( X )
% preselect     Preselect neurons with high chance of having the ramp
% The pca selection could favor certain types of ramps!

[N,M] = size(X);

mu = mean( X,2 );
Xp = X - repmat( mu, 1, M );
C = (1/(M-1))*(Xp*Xp');
[V,D] = eig( C );
D = diag(D);

[~,I] = sort( D, 'descend' );
D = D(I);
V = V( :, I );
V = V( :, 1 );
% Z = (V(:,1)'*X)/sqrt(D(1));

thres = 1/sqrt(N);

pidxs = find( V > thres );
nidxs = find( V < -thres );
pc = V(pidxs);
nc = V(nidxs);

% plot( mean(X(idxs,:),1) )
% pause
% idxs = find( V(:,1) < -thres );
% plot( mean(X(idxs,:), 1) )
% pause

% data = X';
% [M,N] = size(data);
% mn = mean(data,2);
% data = data - repmat(mn,1,N);
% covariance = 1 / (N-1) * data * data';
% [PC, V] = eig(covariance);
% V = diag(V);
% [junk, rindices] = sort(-1*V);
% V = V(rindices)
% PC = PC(:,rindices);
% signals = (PC(:,end)' * data)./V(end);
% 
% % V(end)
% 
% 
% inc_id = find(signals> (2/sqrt(N)) );
% pidxs = inc_id;
% 
% dec_id = find(signals<-(2/sqrt(N)) );
% nidxs = dec_id;
% pc = signals(pidxs);
% nc = signals(nidxs);