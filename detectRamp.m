function result = detectRamp( r, t, varargin )


interval = getArg( varargin, 'Interval', [-1,0] );
rfull = getArg( varargin, 'RFull', r );
tfull = getArg( varargin, 'TFull', t );
result = struct( 'tau1', [], 'tau2', [], 'slope', [], 'p', [] );
dt = abs(tfull(2)-tfull(1));

% First order approximation with Page's or Gallistel
threshold = 5; % Based on simulation
delta = 1; % Not clear what works better

[tau, slope] = pagecusum( r, t, 'distribution', 'normal', ...
                                  'delta', delta, 'threshold', threshold );
%                               
% [tau,slope] = rchange(r,t, 'method', 'confidence');
% [tau, slope] = gallistel( r, t );
% plot( t, r )
% hold on
% plot( [tau tau], [-max(abs(r)) max(abs(r))], 'r--' );
% hold off
% pause
% Non-linear fit

base = mean(r(t<tau));

if isnan(base)
    base = 0;
end

P0 = [slope, tau, tau+dt, base];

yfit = cpnlfit( r, t, 'Type', 'sigmoid', 'P0', P0 );

% Collecting parameters

tau1 = yfit.tau;
tau2 = yfit.tau2;
slope = yfit.slope;
base = yfit.base;
G = yfit.G;
MIN_SAMPLES = 10;

% if G.rsquare < .1 % Preventing spurious fits
%     p = 1; 
if sum(tfull > tau2 ) > MIN_SAMPLES &&  sum(tfull < tau1 ) > MIN_SAMPLES  % performing log likelihood
    % Log likelihood or regression test
    fprintf('Significance as log-likelihood\n')
    pg = @( mu1, sigma, mu2, x ) ((mu2 - mu1)/(sigma^2))*(x - (mu1 + mu2)/2);
    
    X1 = rfull( t < tau1 );
    X2 = rfull( t > tau2 );
    n1 = length(X1);
    n2 = length(X2);
    theta0 = mean( X1 );
    theta1 = mean( X2 );
    sigma = std( [X1,X2] );
%     2*LR
%     p = 1 - chi2cdf( abs(2*LR), 1 )
    

    tobs = (theta1 - theta0)/(sigma*sqrt( 1/n1 + 1/n2 ) );
    p = 2*normcdf( -abs(tobs) );
    
%     figure(3)
%     subplot 121
%     hist( X1, 20 )
%     subplot 122
%     hist( X2, 20 )
%     pause
%     figure(1)
%     
%     ll = min( length(X1), length(X2) );
%     X = abs([X1(1:ll) - X2(1:ll)]); 
% %     figure(3)
% %     clf
% %     
% %     hist(X, 20)
% %     pause
% %     figure(1)
%     [H,p] = ttest(X2, mean(X1));
%     H
%     
%     p

elseif abs(tau2 - tau1) > 2*dt
    % Regression
    fprintf('Significance as regression\n')
    I = find( tfull >= tau1-dt & tfull <= tau2 );
    p = rsignificance( rfull(I), tfull(I), tau1 );
else
    warning('Not enough samples to test significance')
    p = 1; %     
end
%

% if p > 0.05
%     dt = t(2)-t(1);
%     L = @(slope, tau, tau2, base, x) base + ...
%                        (x > tau & x <= (tau2+3*dt)).*(slope*(x - tau)) + ...
%                        (x > (tau2+3*dt)).*(slope*((tau2+3*dt) - tau));
%     g = @(t) L(slope, tau1, tau2, yfit.base, t );
%     plot( tfull, rfull )
%     hold on
%     plot( tfull, g(tfull), 'r' )
%     axis( [-1 0 -max(abs(r)) max(abs(r))] )
%     hold off
%     pause
% end

result.tau1 = yfit.tau;
result.tau2 = yfit.tau2;
result.slope = yfit.slope;
result.base = yfit.base;
result.p = G.rsquare;