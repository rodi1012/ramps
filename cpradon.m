function [tau, slope] = cpradon( r, t )

W = ones( 3 );

% Computation of the matrix
theta = linspace(-pi/2,pi/2,100);
mp = 20*max( abs(t) );
ps = linspace( -mp, mp, 1000 );
kspace = zeros( length(ps), length(theta) );

for i = 1:length(t)
    for k = 1:length(theta)
        cp = t(i)*cos(theta(k)-pi/2) + r(i)*sin(theta(k)-pi/2);            
        q = cp/sin(theta(k));
        I = find( q >= ps, 1, 'last' ) ;
        
        if I < length(ps)
            kspace( I, k ) = kspace( I, k ) + 1;
        end
    end     
end

% Calculation of the estimated slope
% kspace(kspace<10) = 0;
% kspace = conv2( kspace, W, 'valid' );

[m,Is] = max( kspace(:) );
[I,J] = ind2sub( size(kspace), Is );
slope = tan(theta(J));
tau = ps(I);

% subplot 121
% plot( t, r )
% hold on 
% plot( t, slope*(t - tau), 'k', 'linewidth', 2 )
% hold off
% subplot 122
% imagesc( 'XData', theta, 'YData', ps, 'CData', kspace )
%  

end