% Analyses

%% Moving avearge
addpath( '../common' )

close all
clc
N = 100;
n_trials = 100;
n_bins = 100;
interval = [-1, 0];
[data, params] = simdata( N, n_trials , 'sigmoid' );

for i = 1:N
    trials = data{i};
    [r,t] = psth( trials, n_bins, interval, false );
    
    M = 20;
    tp = linspace(interval(1), interval(2), M);
    tm = (tp(2:end) + tp(1:end-1))/2;
    rp = zeros(1,M-1);
    
    for k = 1:M-1
        I = find( t >= tp(k) & t < tp(k+1) );
        rp(k) = mean( r(I) );
    end
    
    plot( t, r )
    hold on
    plot( tm, rp, 'r' )
    hold off
    pause 
end

%% Application example data
close all
clc

N = 100;
n_trials = 100;
n_bins = 100;
interval = [-1, 0];

map = [165,0,38;
        215,48,39;
        244,109,67;
        253,174,97;
        254,224,144;
        224,243,248;
        171,217,233;
        116,173,209;
        69,117,180;
        49,54,149];
map = flipud(map)/255;

figures = struct( 'example_simulation_ramps', @figure1, ...
                  'example_simulation_dist', @figure2 );
              
rateFn = @(trials) psth( trials, n_bins, interval, false );
% rateFn = @(trials) sdf( trials, 'asymmetric', n_bins, interval, 0.05 );
detectionFn = @(r,t) detectRamp(r, t, 'Interval', interval);
        

              
respool = doAnalysis( data, ...
                    'Colormap', map, ...
                    'Figures', figures, ...
                    'Rate', rateFn, ...
                    'Detection', detectionFn, ... 
                    'Interval', interval, ... 
                    'Cut', [],...
                    'NBins', n_bins, ...
                    'ZScore', true,...
                    'PreSelect', false);
                
for k = 1:length(respool.ps)
    r = respool.Xramp(k,:);
    t = respool.t;
    plot( t, r )
    hold on
    plot( [respool.taus(k), respool.taus(k)], [min(r), max(r)], 'r--' )
    plot( params.tau(k), 0, 'r*' )
    title(sprintf('p=%.2f', respool.ps(k)))
    hold off
    pause
end

%% Analysis simulated data

close all
clc

N = 100;
n_trials = 100;
n_bins = 100;
interval = [-1,0];
map = jet;
map = [165,0,38;
        215,48,39;
        244,109,67;
        253,174,97;
        254,224,144;
        224,243,248;
        171,217,233;
        116,173,209;
        69,117,180;
        49,54,149];
map = flipud(map)/255;
[rdata, params] = simdata( N, n_trials , 'sigmoid', 'TauDistribution', 'uniform' );


sdata = struct;
sdata.('simulation') = rdata;

% Figures
% figures = struct( 'sim_dist_final_method', @figure1, ...
%                   'sim_dist_distributions_finalmethod', @figure2 );
              
figures = struct( 'sim_sigmoid_final_method', @figure1, ...
                  'sim_sigmoid_distributions_finalmethod', @figure2, ... );
                  'sim_sigmoid_derivatives_finalmethod', @figure3 );
              
preprocess = 'psth->(ratefilter)->intervalcut';
% detection = 'pagecusum->twostepfit->tsignificance->smooth';
detection = 'pagecusum->cpnlfit->tsignificance->smooth';
analysis = [preprocess, '->', detection];

result = feach( sdata, analysis, 'Interval', interval, ... 
                                'NBins', n_bins, ...
                                'ZScore', false, ...
                                'Cut', [-1,0], ...
                                'PreSelect', false, ...
                                'Pool', 'Tau,Slope,Time', ...
                                'distribution', 'poisson', ...
                                'FitType', 'pwlinear');
                            
 pfigures( result, figures, 'Colormap', map, ...
                            'OriginalTaus', params.tau, ...
                            'SortBy', 'tau', ...
                            'Interval', interval );



%% Preload
rdata = preload();

%% Loading the data

addpath('../lib')

% Load data
[data, holding] = loadData( rdata, 'Interval', [-1.1,1.1], ...
                            'TrialTypes', 'correctgo', ...
                            'Align', 'tone', ...
                            'Regions', {'Str', 'Str_1', 'Str_2', 'GP', 'GP<protos', 'GP<arkys', 'SNr', 'STN', 'thal'}, ...
                            'Covariate', 'holding');
%% Checking the data
close all
region = 'Str';
X = data.(region);
n_bins = 100;
interval = [-1, 0];

for i = 1:length(X)
    trials = X{i};
    subplot 121
step = 0.01;
    raster( trials )
    title( sprintf('Neuron %d', i) )
    subplot 122
    [r,t] = psth( trials, n_bins, interval, false );
    [r,t] = sdf( trials, 'asymmetric', n_bins, interval, 0.05 );
    plot( t, r ); 
    title( sprintf('Neuron %d', i) )
    pause
end
%% Analysis on the data - mean over triasl - Nlfit - piecewise - psth


%% Analysis on the data - mean over triasl - Nlfit - sigmoid - psth


%% Analysis on the data - mean over trials - rateChange - psth


%% Ramps - on data
close all
clc
addpath( 'bars' );

% Parameters
map = [165,0,38;
        215,48,39;
        244,109,67;
        253,174,97;
        254,224,144;
        224,243,248;
        171,217,233;
        116,173,209;
        69,117,180;
        49,54,149];
map = flipud(map)/255;
% map = bone;
step = 0.01;
n_bins = 1/step;
interval = [-1,1];

% Figures
figures = struct( 'final_method', @figure1, ...
                  'dihttps://uk.mathworks.com/matlabcentral/answers/239776-error-using-lsqncommon-line-67stributions_finalmethod', @figure2, ... );
                  'significances_finalmethod', @figure4 );
              
% preprocess = 'psth->(ratefilter)->intervalcut';
preprocess = 'psth->(ratefilter)->intervalcut';
% detection = 'pagecusum->twostepfit->tsignificance->smooth';
detection = 'pagecusum->cpnlfit->tsignificance->smooth';

analysis = [preprocess, '->', detection];

X = data.Str;
tdata = struct('test', {X}) ;

result = feach(tdata, analysis, 'Interval', interval, ... 
                                'NBins', n_bins, ...
                                'ZScore', false, ...
                                'Cut', [-1,0], ...
                                'PreSelect', false, ...
                                'Pool', 'Tau,Slope,Time,R2,PVar,PChange,PRamp', ...
                                'distribution', 'normal', ...
                                'FitType', 'sigmoid');
                            
 pfigures( result, figures, 'Colormap', map, ...
                            'SortBy', 'tau', ...
                            'Interval', interval);

%% Test of single trial methods

%% Checking assumptions
close all
clc

step = 0.01;
n_bins = 1/step;
interval = [-1.1,1.1];

analysis = 'psth->(ratefilter)->normncut->bilateralfilt->simplify';
% analysis = 'psth->normncut->wvfilt->simplify';
% analysis = 'psth->normncut->barsfilt->simplify';


N = 100;
n_trials = 100;
[rdata, params] = simdata( N, n_trials , 'sigmoid', 'TauDistribution', 'uniform' );
sdata = struct;
sdata.('simulation') = rdata;

X = data.Str;
tdata = struct('test', {X}) ;

figures = struct('ci_vs_ff', @(h)plot([0, 0], [-1 1]) , ...
                 'renormalized', @(h)plot([0, 0], [-1 1]) );

result = feach(tdata, analysis, 'Draw', true,...
                                'Interval', interval, ... 
                                'NBins', n_bins, ...
                                'Cut', [-1,0],...
                                'Kernel', 'asymmetric-right', ...
                                'distribution', 'normal', ...
                                'Sigma', 0.01, ...
                                'FitType', 'sigmoid');
pfigures( result, figures );


%% Analysis of data single trials

close all
clc

step = 0.01;
n_bins = 1/step;
interval = [-1.1,1.1];

analysis = 'psth->(ratefilter)->normncut->pwoptfit';

N = 100;
n_trials = 100;
[rdata, params] = simdata( N, n_trials , 'sigmoid', 'TauDistribution', 'uniform' );
sdata = struct;
sdata.('simulation') = rdata;

X = data.Str;
tdata = struct('test', {X}) ;

figures = struct('ci_vs_ff', @(h)plot([0, 0], [-1 1]) , ...
                 'renormalized', @(h)plot([0, 0], [-1 1]) );

result = feach(tdata, analysis, 'Draw', true, ...
                                'Interval', interval, ... 
                                'NBins', n_bins, ...
                                'Cut', [-1,0],...
                                'Kernel', 'asymmetric-right', ...
                                'distribution', 'normal', ...
                                'Sigma', 0.01, ...
                                'FitType', 'sigmoid');
pfigures( result, figures );

%% Final findramps algorithm

close all
clc

step = 0.01;
n_bins = 1/step;
interval = [-1.1,1.1];

analysis = 'psth->(ratefilter)->intervalcut->zscore->findramps';

N = 20;
n_trials = 100;
[rdata, params] = simdata( N, n_trials , 'sigmoid', 'TauDistribution', 'uniform' );
sdata = struct;
sdata.('sim1') = rdata;
sdata.('sim2') = rdata;
% 
X = data.SNr;
tdata = struct('SNr', {X}) ;


result = feach(data, analysis, 'Draw', false, ...
                                'Interval', interval, ... 
                                'NBins', n_bins, ...
                                'Cut', [-1,0],...
                                'FitType', 'sigmoid', ...
                                'Pool', 'Taus,Alphas,Time,NumberChangePoints,Type');

%% GRaphics

% Parameters
map = [165,0,38;
        215,48,39;
        244,109,67;
        253,174,97;
        254,224,144;
        224,243,248;
        171,217,233;
        116,173,209;
        69,117,180;
        49,54,149];
map = flipud(map)/255;

figures = struct('strong_pattern_type', @figStrongPatternTypes, ...
                 'weak_pattern_type', @figWeakPatternTypes, ...
                 'no_pattern_types', @figNoPatternTypes);
pfigures( result, figures, 'Colormap', map );




















