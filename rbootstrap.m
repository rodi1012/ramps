function [L,U] = rbootstrap( r, t, fn, theta, varargin )
% Bootstrap just for tau#
% theta is the current MLE estimate

type = getArg( varargin, 'type', 'parametric' );
method = getArg( varargin, 'method', 'SE' );
slope = getArg( varargin, 'slope', 0 );
G = getArg( varargin, 'G', 1000 );
n_bins = 200;
interval = getArg( varargin, 'interval', [0,1] );

L = [];
U = [];

switch type
    case 'parametric'
        W = zeros(1,G);
        [data, params] = simdata( G, 100 , 'ramp', 'FixRates', true, 'tau', theta, 'slope', slope );

        for i = 1:G    
            x = data{i};  
            [r_g, t] = psth( x, n_bins, interval, false ); 
            [tau, slope] = fn( r_g, t ); 
            W(i) = tau;
        end

        if strcmp(method, 'SE')
            Wm = mean(W);            
            SE = sqrt(sum( (W - Wm).^2 )/(G-1));
            % The mean or the estimate?
            L = Wm - 2*SE;
            U = Wm + 2*SE;
        else        
            O = sort(W, 'ascend');
            I025 = ceil(0.025*G);
            I975 = ceil(0.975*G);
            L = O(I025);
            U = O(I975);
        end     
    case 'non-parametric'
        error('Not ready or not applicable.')
end