function [lr, params] = logtx( r, varargin )
% LOGTX applies the log transform to the data

draw = getArg( varargin, 'Draw', false );
t = getArg( varargin, 'Time', false );
lr = log(r);

if draw
    subplot 121
    plot( t, r )
    subplot 122
    plot( t, lr, 'r' );
    hold off
%     axis( [-1 0 -10 10] )
    pause
end


params = struct;
