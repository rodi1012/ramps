function [y, params] = twostepfit( y, varargin )
% Iterative fit
    t = getArg( varargin, 'Time', 1:length(y) );
    draw = getArg( varargin, 'Draw', true );
    tau0 = getArg( varargin, 'Tau', [] );
    interval = getArg( varargin, 'Interval', [] );
    B = randi(20,2,1);
    
    function r = g( tau )
        u = @(t) (t > tau).*(t - tau);
        x = u(t);        
%         L = @(theta) -sum( y.*(theta(1) + theta(2)*x) - exp(theta(1) + theta(2)*x));
        L = @(theta) -sum( y.*log(theta(1) + theta(2)*x) - theta(1) - theta(2)*x);
        theta0 = B;
        opts = optimset('Display','off');        
        [B, r] = fminsearch( L, theta0, opts );
    end
    
    if isempty( tau0 )
        nn = 1:length(t);
        mu = cumsum(y)./nn;
        sigma = sqrt(cumsum(y.^2)./nn - mu.^2);
        I = find( abs( y - mu ) > 2*sigma, 1, 'first' );
        tau0 = t(I); %first estimate
    end
    
    opts = optimset('Display','off', ...
                    'MaxFunEvals', 1e3, ...
                    'MaxIter', 1e3 );    
    tau = fminsearch( @g, tau0, opts );    
%     tau = fminbnd( @g, interval(1), interval(2), opts );

    if tau < interval(1) || tau > interval(2)
        tau = interval(2);
    end
    
    params = struct( 'Tau', tau, 'B0', B(1), 'Slope', B(2) );
    
    if draw
        plot( t, y, 'k' )
        hold on
        plot( [tau0, tau0], [min(y) max(y)], 'r--' )
        L = @(t) B(1) + (t > tau).*(B(2)*(t - tau));
        plot( t, L(t), 'b')
        plot( tau, L(tau), 'b*' );        
        hold off
        pause       
    end
    
end


