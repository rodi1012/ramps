function figWeakPatternTypes( h, varargin )
% Plots the heatmaps of the firing rates sorted by starting point

figure(h)
clf(h)
region = getArg( varargin, 'Region', [] );
X_ramp = getArg( varargin, 'X_ramp', [] );
taus = getArg( varargin, 'Taus', [] );
alphas = getArg( varargin, 'Alphas', [] );
numChangePoints = getArg( varargin, 'NumberChangePoints', [] );
t = getArg( varargin, 'Time', [] );
type = getArg( varargin, 'Type', [] );
map = getArg( varargin, 'Colormap', jet );
interval = getArg( varargin, 'Interval', [] );

t = t{1};
I_data = find( arrayfun(@(i)strcmp( type{i}, 'weak_pattern' ), 1:length(type)) );

I_0_cp = [];
I_1_cp = [];
tau_1_cp = [];
alpha_1_cp = [];
I_2_cp = [];
tau_2_cp = [];
alpha_2_cp = [];
I_more_cp = [];
tau_more_cp = [];
alpha_more_cp = [];

% Plotting Strong patterns
for i = 1:length(I_data)
   cps = numChangePoints{I_data(i)};
   ctaus = taus{I_data(i)}; 
   calphas = alphas{I_data(i)};
           
   switch cps
       case 0
           I_0_cp = [I_0_cp, I_data(i)];
       case 1
           I_1_cp = [I_1_cp, I_data(i)];           
           tau_1_cp = [tau_1_cp, ctaus(2)];
           alpha_1_cp = [alpha_1_cp, sum(calphas)];
       case 2
           I_2_cp = [I_2_cp, I_data(i)];      
           tau_2_cp = [tau_2_cp, ctaus(2)];
           alpha_2_cp = [alpha_2_cp, sum(calphas)];
       otherwise
           I_more_cp = [I_more_cp, I_data(i)];
           tau_more_cp = [tau_more_cp, ctaus(end-1)];
           alpha_more_cp = [alpha_more_cp, sum(calphas)];
   end
end

function plotRate( I_cp, tau_cp, alpha_cp )
    if isempty( I_cp )
        return
    end
    
    pI = find(alpha_cp >= 0 );
    nI = find(alpha_cp < 0 );
    Xp = X_ramp( I_cp(pI), : );
    Xn = X_ramp( I_cp(nI), : );
    
%     for i = 1:size(Xp,1)
%         plot( t, Xp(i,:) )
% axis( [interval(1) interval(2) 0 1 ] )
%         pause
%     end
    
    plot( t, mean( Xp, 1 ), 'b' )
    hold on    
    plot( t, mean( Xn, 1 ), 'r' )
    hold off
    
    axis( [interval(1) interval(2) -3 3 ] )
    xlabel( 'Time (ms)' ) 
end

function plotSub( I_cp, tau_cp, alpha_cp )
    if isempty( I_cp )
        return
    end
    
    pI = find(alpha_cp >= 0 );
    nI = find(alpha_cp < 0 );
    ctausp = tau_cp(pI); 
    ctausn = tau_cp(nI);
    [~,pI_s] = sort( ctausp );
    [~,nI_s] = sort( ctausn );
    Xp = X_ramp( I_cp(pI), : );
    Xn = X_ramp( I_cp(nI), : ); 
    ctaus = [ctausp(pI_s), ctausn(nI_s)];
    X = [Xp(pI_s,:);Xn(nI_s,:)];
    imagesc( 'XData', t, 'YData', 1:length(I_cp), 'CData', X )
    hold on
    plot( ctaus, 1:length(I_cp), '.', 'markersize', 10, 'color', [0 0 0] )
    hold off
    
    if length(I_cp) > 1
        axis([interval(1) interval(2) 1 length(I_cp)])
    end    
    
    colorbar
    colormap(map)
end

subplot(2,3,1)
% 1 Changing point
plotSub( I_1_cp, tau_1_cp, alpha_1_cp );
title( 'Weak patterns: 1 cp' )

subplot (2,3,4)
plotRate( I_1_cp, tau_1_cp, alpha_1_cp );

subplot(2,3,2)
% 2 changing points
plotSub( I_2_cp, tau_2_cp, alpha_2_cp );
title( 'Weak patterns: 2 cp' )

subplot(2,3,5)
plotRate( I_2_cp, tau_2_cp, alpha_2_cp );

subplot(2,3,3)
% More changing points
plotSub( I_more_cp, tau_more_cp, alpha_more_cp );
title( 'Weak patterns: >=3 cp' )

subplot (2,3,6)
plotRate( I_more_cp, tau_more_cp, alpha_more_cp );

end
