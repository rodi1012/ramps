function [r, lambda, ilambda] = genproc( t, alpha, taus )

n = length(alpha);
F = @(t) max(t,0);
H = @(t) min([t(ones(1,n)); taus(2:end)]);
L = @(t) alpha*F(H(t) - taus(1:end-1))'*30;
dt = t(2)-t(1);
lambda = arrayfun( @(i)L((t(i+1)+t(i))/2), 1:length(t)-1);
ilambda = arrayfun( @(i)L((t(i+1)+t(i))/2), 1:length(t)-1);
r = arrayfun( @(i)poissrnd( lambda(i) ), 1:length(lambda) );