function [data, params] = simdata( numNeurons, numTrials, type, varargin )
% simdata   Simulate poisson ramping processes for trials and neurons
% numNeurons - Number of neurons
% numTrials - Number of trials
% type - Either step or ramp
% The output has the format:
% data - [ trial_num, data ]

interval = [-1, 0];
rates = [5, 40];    

fixRates = getArg( varargin, 'FixRates', false );
tau = getArg( varargin, 'tau', [] );
slope =  getArg( varargin, 'slope', 0 );
taudist = getArg( varargin, 'TauDistribution', 'uniform' );
srates = rates;

function s = nextTrial( lambdat )
% Generates nonhomogeous poisson spikes
    lmax = max(rates);
    p = poissrnd(lmax);
    % generating p uniform spikes
    times = rand(p, 1)*(interval(2) - interval(1)) + min(interval);    
    % Checking statistic    
    rs =  rand(p,1) < lambdat(times)/lmax;
    % Adding spikes
    s = times(rs);
end

switch taudist
    case 'uniform'
        gentau = @(mu) rand - 1;
    case 'normal'
        gentau = @(mu) randn*0.1 + mu;
end

data = cell(1, numNeurons);
params = struct( 'tau', [], 'slope', [] );

for i = 1:numNeurons
    switch type
        case 'step'
            trials = [];           
            srates = rates( randperm(2) );
            
            for j = 1:numTrials
                tau = gentau(-0.4);                
                lambdat = @(t) srates(1)*(t <= tau) + (t > tau)*srates(2);
                slist = nextTrial( lambdat );                
                trials = [trials; ones(size(slist,1),1)*j, slist];
            end            
        case 'ramp'
            trials = [];
            
            if ~fixRates || isempty(tau)
                tau = gentau(-0.4);            
                srates = rates( randperm(2) ); 
                slope = (srates(2)-srates(1))/(interval(2) - tau);
            end
            
            params.tau = [params.tau, tau];            
            params.slope = [params.slope, slope];
            lambdat = @(t) srates(1) + (t > tau).*(slope*(t - tau));
            
            for j = 1:numTrials                
                slist = nextTrial( lambdat );                
                trials = [trials; ones(size(slist,1),1)*j, slist];
            end
        case 'sigmoid'
            trials = [];
            
            if ~fixRates || isempty(tau)
                tau1 = gentau(-0.4);            
                tau2 = rand*(interval(2) - tau1) + tau1;
                srates = rates( randperm(2) ); 
                slope = (srates(2)-srates(1))/(tau2 - tau1);
            end
            
            params.tau = [params.tau, tau1];            
            params.slope = [params.slope, slope];
            lambdat = @(t) srates(1) + ...
                          (t > tau1 & t <= tau2).*(slope*(t - tau1)) + ...
                          (t > tau2).*slope*(tau2 - tau1);
            
            for j = 1:numTrials                
                slist = nextTrial( lambdat );                
                trials = [trials; ones(size(slist,1),1)*j, slist];
            end
    end

    data{i} = trials; 
end

end % function