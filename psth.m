function [h, t] = psth( X, nbins, I, draw )
% Computes the psth from a set of trials
% The format of X is: two columns, 

dt = (I(2) - I(1))/nbins;
edges = I(1):dt:I(2);
h = zeros(1,nbins);
n = max(X(:,1));
% Maps
unitmap = @(x) (x - I(1))/(I(2) - I(1));
binmap = @(x) ceil( x*nbins ) + ( edges(ceil( x*nbins )) < x ) - 1;

% Computing the histogram
for i = 1:size(X,1)
    c = unitmap( X( i,2 ) );
    ind = binmap( c );
    h( ind ) = h( ind ) + 1;
end

% Plotting the results
t = (edges(1:end-1) + edges(2:end))/2;

if draw 
    plot( t, h, 'linewidth', 2.0 )
    xlabel( 'time' )
    ylabel( 'counts' )
end