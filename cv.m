function [X, params] = cv( X, varargin )
% CV    Computes the coefficient of variations by applying the operational
% time approach
% Source:
% Nawrot, M. P., Boucsein, C., Molina, V. R., Riehle, A., Aertsen, A., & Rotter, S. (2008). 
% Measurement of variability dynamics in cortical spike trains. Journal of neuroscience methods, 169(2), 374-390.

n_bins = getArg( varargin, 'NBins', 100 );
interval = getArg( varargin, 'Interval', [] );
draw = getArg( varargin, 'Draw', false );

[r, p] = sdf( X, 'Kernel', 'triangular', ...
                 'Sigma', 0.05, ...
                 'Interval', interval, ...
                 'NBins', n_bins );
t = p.Time;
dt = t(2)-t(1);
t_op = cumsum( r )*dt;
% spline interpolation
t_sp = spline( t, t_op );

if draw
    clf
    subplot 231
    plot( t,  t_op )
    subplot 234
    plot( t, r )
    subplot( 2, 3, [2, 5] )
    raster( X )
    subplot( 2, 3, [3, 6] )
    raster( [X(:,1) ppval(t_sp, X(:,2))] );
    pause
end

params = struct;