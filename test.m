%% Test random data and binning

close all 
N = 16;
I = [-1, 0];
edges = linspace( I(1), I(2), N );

s = rand(1,1000)*(I(2) - I(1)) + I(1);
bin1 = zeros(1,length(s));
bin2 = zeros(1,length(s));

for i = 1:length(s)
    bin1(i) = find( s(i) > edges(1:end-1) & s(i) <= edges(2:end) );
    x = (s(i) - I(1))/(I(2) - I(1));
    k = ceil(x*N);
    bin2(i) = k + (edges(k) < s(i)) -1;    
end

bin1(1:10)
bin2(1:10)
plot( bin1- bin2 )
hold on

%% Asymmetric function

b = 3;
T = 100;
t = linspace( 0, b, T );
dt = b/T;
tg = .8; td = .3; 
A = (td + tg)/(td^2);
K = @(t) A*(1 - exp(-t/tg)).*exp(-t/td);
close all;plot( t, K(t) )

sum(K(t))*dt
sum(K(t).*t)*dt

a = 1/tg;
b = 1/td;

mu = A*(a/(b*(a + b)))*(1/(a+b) + 1/b)

%% 

close all
while true
x = linspace(-5,5,100);
b1 = 5;
b2 = 3.4;
plot( x, b1+b2*x)
xx = rand*4 - 2;
p = [xx, b1 + b2*xx + rand*20-10];
plot(p(1),p(2),'r.','markersize',15)
plot( x, b1+b2*x)
hold on
plot(p(1),p(2),'r.','markersize',15)

xp = (p(1) + p(2)*b2 - b1*b2)/(1 + b2^2);

d = @(q) sqrt((p(1) - q(1))^2 + (p(2) - q(2))^2);

[~,I] = min( arrayfun( @(i) d( [x(i), b1 + b2*x(i)] ), 1:length(x) ) );

plot( xp, b1 + b2*xp, 'k.', 'markersize', 15 )

plot( x(I), b1 + b2*x(I), 'g.', 'markersize', 15 )

e = [-b2/b1, 1/b1];
L = [x.*e(1); x.*e(2)];

e*[x;b1 + b2*x]

plot( L(1, :), L(2, :), 'k--' ) 
plot( [0, e(1)],[0, e(2)], 'r', 'linewidth', 2 )
plot( e(1),e(2), 'r*', 'linewidth', 2 )
plot( [zeros(1,length(x)); x]', [zeros(1,length(x)); b1+b2*x]', 'k')
axis equal
hold off
pause
end

%% Lines

close all
thetas = linspace(-pi/2,pi/2,50);
q = linspace(-4,4,9);

for k = 1:length(q)   

    for i = 1:length(thetas) 
        phi = thetas(i) - pi/2;
        q(k)
        thetas(i)
        p = q(k)*sin(thetas(i));
%         p = cos(theta(i))*sin(theta(i));
        x = linspace(-4,4,20 );
        na = cos(phi);
        nb = sin(phi);
        y = p/nb - x*na/nb;
        plot( x,y,'b' )
        hold on
        plot( [0 p*na], [0 p*nb], 'k--' )
        hold off
        grid on
        axis([-4 4 -4 4])

        pause
    end
    hold off
end

%% KInematic space

close all

taus = linspace( -1,0,50 );
W = ones( 3 );
figure
pause

for j = 1:length(taus)
    points= [];
    tau = taus(j);
    t = linspace( -1, 0, 100 );
    s = -1;
    L = @(t) (t > tau).*s.*(t - tau);
    y = L(t) + rand(size(t))*.1;

    figure(1)
    subplot 121
    plot( t, y, '.' )

    theta = linspace(-pi/2,pi/2,100);
    mp = max( abs(t) )*(abs(s) + 1);R = zeros( N, N );

for i = 1:N
    for j = 1:N        
        R(i,j) = abs(Y(i) - Y(j)) < 0.1;
    end
end

R
imagesc(R)

    ps = linspace( -mp, mp, 1000 );
    kspace = zeros( length(ps), length(theta) );

    for i = 1:length(t)
        for k = 1:length(theta)
            cp = t(i)*cos(theta(k)-pi/2) + y(i)*sin(theta(k)-pi/2);            
            q = cp/sin(theta(k));
%             if( abs(theta(k)) < .01 )
%                  q
%             end
            points = [points; theta(k), q];
            I = find( q >= ps, 1, 'last' ) ; 
            if I < length(ps)
                kspace( I, k ) = kspace( I, k ) + 1;
            end
        end     
    end 
    
    kspace = conv2( kspace, W, 'valid' );
    kspace(kspace<10) = 0;
    
    cm = [0;0]
    M = sum(sum(kspace));
    
    for i = 1:size(kspace,1)
        for k = 1:size(kspace,2)
            cm = cm + kspace(i,k)*[ps(i);theta(k)]/M;
        end
    end
    
    
        
    [m,Is] = max( kspace(:) );
    [I,J] = ind2sub( size(kspace), Is )
    hold on 
    plot( t, tan(theta(J))*(t - ps(I)), 'k', 'linewidth', 2 )
    hold off
%     axis([min(t) max(t) 0 s])
    
    subplot 122
    
    imagesc( 'XData', theta, 'YData', ps, 'CData', kspace )
    hold on
    plot( cm(2), cm(1), 'r.', 'markersize', 20 )
    hold off
    colormap bone
%     set(gca,'Ydir','Normal')
    ylabel( 'p' )
    xlabel( '\theta' )
%     
%     figure(2)
%     plot( points(:,1), points(:,2), 'k.' )
%     axis([-pi/2 pi/2 -mp mp])
    pause
end
%%
close all
clc

taus = linspace( -1,0,50 );
figure
pause

for j = 1:length(taus)
    points= [];
    tau = taus(j);
    t = linspace( -1, 0, 100 );
    s = .5;
    L = @(t) (t > tau).*s.*(t - tau);
    y = L(t) + rand(size(t))*.1;

    figure(1)
    subplot 121
    plot( t, y, '.' )

    theta = linspace(0,2*pi,100);
    mp = max( abs(t) )*(s + 1);
    ps = linspace( -mp, mp, 1000 );
    kspace = zeros( length(ps), length(theta) );
regression
    for i = 1:length(t)
        for k = 1:length(theta)
            cp = t(i)*cos(theta(k)) + y(i)*sin(theta(k));            
            q = cp;
%             if( abs(theta(k)) < .01 )
%                  q
%             end
            points = [points; theta(k), q];
            I = find( q >= ps, 1, 'last' );
            
            if isempty( I )
                I = length(ps)
            end
            
            kspace( I, k ) = kspace( I, k ) + 1;
        end     
    end
    
    [m,Is] = max( kspace(:) );
    [I,J] = ind2sub( size(kspace), Is );
    hold on 
    plot( t, tan(theta(J)+pi/2)*(t - ps(I)/sin(theta(J)+pi/2)), 'k', 'linewidth', 2 )
    hold off
    axis([min(t) max(t) 0 s])
    subplot 122
%     kspace(kspace<5) = 0;
    imagesc( 'XData', theta, 'YData', ps, 'CData', kspace, [0,10] )
    colormap bone
    hold on
    plot( theta, tau.*cos(theta), 'color', 'r', 'linewidth', 1)
    
    hold off
%     set(gca,'Ydir','Normal')
    ylabel( 'p' )
    xlabel( '\theta' )
    
%     figure(2)
%     plot( points(:,1), points(:,2), 'k.' )
%     axis([-pi/2 pi/2 -mp mp])
    pause(.1)
end

%% 
close all
clear all
clc

a = -1;
b = 0;
n = 200;
t = linspace(a,b,n+1);
K = @(t) ceil(n*(t - a)/(b-a)) + 1;
KK = @(s)K(s) - (s<t(K(s)));

s = rand(1,10000)*(b-a) + a;
pos1 = zeros( size(s) );
pos2 = zeros( size(s) );

for i = 1:length(t)-1
    I = find( s >= t(i) & s < t(i+1) );
    pos1(I) = i;
end

for i = 1:length(s)
    I = KK(s(i));
    pos2(i) = I;
end

plot( pos1-pos2, 'b' )

%% Sigmoid function
close all
x = linspace( -1, 1, 100 );
L = 1;
x0 = 0;
ks = linspace(-10,10,100);
b = 0.3;

for k = ks
    f = @(x) L./(1 + exp(-k*(x - x0))) + b;
    df = @(x) (k/L).*(L - f(x)).*f(x);
    ddf = @(x) (k/L).*(1-2*f(x)).*df(x);
    
    s = k*L/4;
    fprintf('Derivatives. Theor: %.3f, actual: %.3f\n', s, df(x0))
    
    g = f(x);
    dg = diff(g);
    ddg =diff(dg);
    plot( x, f(x),'k' )
    hold on
    s = k*(2-L)/4;
    plot( x, s*(x-x0) + f(x0) )
%     plot( x, df(x)/max(abs(df(x))),'b' )
%     plot( x(1:end-1)+1/100, dg/max(abs(dg)), 'b--' )
%     plot( x, ddf(x)/max(abs(ddf(x))), 'r' )
%     plot( x(1:end-2) + 2/100, ddg/max(abs(ddg)), 'r--' )
    if s > 0
        plot( -L/(2*s)+x0, b, 'r*')
        plot( [-1 1], [b, b], 'r--' )
    else
        plot( L/(2*s) + x0, L+b, 'r*' )
        plot( [-1 1], [L+b, L+b], 'r--' )
    end
    
%     
%     d3f = diff(ddf(x));
%     [pks, locs] = findpeaks( abs(d3f) );
%     plot( x(locs), sign(d3f(locs)).*pks/max(abs(d3f)), 'r*' )
    
    hold off
    axis([-(x0+1) (x0+1) -1.5 1.5])
    pause(.1)
end

%% z score%% Piecewise fittitng

fp = @(t) max(0, t);
L = @(t) 

close all
N = 10;
n_trials = 100;
n_bins = 100;
interval = [-1, 0];
[data, params] = simdata( N, n_trials , 'ramp' );

dt = 1/n_bins;

for i = 1:N
    trials = data{i};
    [r,t] = psth( trials, n_bins, interval, false );
    
    mus = cumsum( r )./(1:length(r));
    mu = mean( r );
    sigma = std( r );
    
%     r = (r - mu)/sigma;
    
    plot( t, r )
    hold on
    size(t)
    size(mus)
    
    plot( t, mus, 'r--' )
    plot( [interval(1), interval(2)], [0, 0], 'k--' )
    hold off
    pause
    
end

%% Raster and rate

close all
clc
N = 100;
n_trials = 30;
n_bins = 50;
interval = [-1, 0];
[data, params] = simdata( N, n_trials , 'sigmoid' );
edges = linspace(-1, 0, n_bins+1);

for i = 1:N
    X = data{i};
    params.tau(i) 
    params.slope(i) 
    subplot 311
    raster(X)
    axis([interval(1) interval(2) 0 n_trials])
    grid on
    subplot 312
    [r,t] = psth( X, n_bins, interval, true );
    hold on
    plot( [params.tau(i) params.tau(i)], [0 max(r)], 'r--')
    hold off
    subplot 313
    [rg,t] = sdf( X, 'gaussian', n_bins, interval, 0.05, 'range', 'valid' );
    [ra,t] = sdf( X, 'asymmetric', n_bins, interval, 0.05 );
    [r,t] = sdf( X, 'step', n_bins, interval, 0.05 );
    plot( t, rg, 'k' )
    hold on
    plot( t, ra, 'b' )
    plot( t, r, 'r' )
%     legend('gaussian', 'asymmetric', 'step')
    hold off
    pause
end

%% 

close all
clc 

P = 1;
% rho = rand(1,P).*0.5;
rho = @(k)(k > tauk)*0 + 0.5;
N = 100;
tauk = 40;
Y = zeros( 1, N );
drift = @(k) (k > tauk)*(2*(k- tauk));
% drift = @(k) 0;
% err = @(k) (k > tauk)*5 + 1;
err = @(k) 5;

for i = P+1:N
    Y(i) = drift(i) + sum(rho(i).*Y(i-P+1:i)) + randn*err(i);
end

subplot 121
plot(Y)
subplot 122

R = zeros( N, N );

for i = 1:N
    for j = 1:N        
        R(i,j) = abs(Y(i) - Y(j)) < 0.1;
    end
end

R
imagesc(R)



%% Radon again
close all
clc
M = 100;
T = 60;
I = zeros( M, M );

mu = 20;
for i = 1:M
    if i < T 
        I(mu + randi(5), i ) = 1;
    else
        I(mu + randi(5) + (i-T+1), i ) = 1;
    end
end

figure
imagesc(I)
figure
theta = 0:180;
[R,xp] = radon( I, theta );

imshow(R,[],'Xdata',theta,'Ydata',xp,'InitialMagnification','fit')
xlabel('\theta (degrees)')
ylabel('x''')
colormap(gca,hot), colorbar

%% Radon from an image

close all
clc 

P = 1;
% rho = rand(1,P).*0.5;
rho = 0.5;
N = 100;
tauk =20;
Y = zeros( 1, N );
drift = @(k) (k > tauk)*(2*(k- tauk));

for i = P+1:N
    Y(i) = drift(i) + sum(rho.*Y(i-P+1:i)) + randn*5;
end

M = 40;
Im = zeros( M, N );
b = max(Y);
a = min(Y);
edges = linspace(a,b,M);

for i = 1:N
    idx = find( edges <= Y(i), 1, 'last' );
    Im(idx, i ) = 1;
end

imagesc(Im)

figure
ss the course. I will be checking this regularly over the next few months and will update your record accordingly but if you have time to just drop me a line and let 
[F,Fpos,Fangles] = fanbeam(Im,250);
figure
imshow(F,[],'XData',Fangles,'YData',Fpos,...
            'InitialMagnification','fit')
axis normal
xlabel('Rotation Angles (degrees)')
ylabel('Sensor Positions (degrees)')
colormap(gca,hot), colorbar
% 
% iptsetpref('ImshowAxesVisible','on')
% theta = 0:180;
% [R,xp] = radon( Im, theta );
% imshow(R,[],'Xdata',theta,'Ydata',xp,'InitialMagnification','fit')
% xlabel('\theta (degrees)')
% ylabel('x''')
% colormap(gca,hot), colorbar

%%
close all
clc
N = 50;
n_trials = 100;
n_bins = 100;
interval = [-1, 0];
[data, params] = simdata( N, n_trials , 'ramp' );
X = [];

for i = 1:N
    
    [r,t] = tradrate(data{i}, 'WindowWidth', 0.05, 'StepSize', 0.005, 'Interval', interval); 
    r = smooth( r, 5 );
    X = [X; r];
end

subplot 121
imagesc( X )

Xp = X( :, 2:end );
X = X( :, 1:end-1 );

[U,S,V] = svd( X );
A = (U'*Xp*V')*pinv(S);
[W,D] = eig(A);
phi = Xp*V'*pinv(S)*W;

subplot 122
imagesc(abs(phi))

figure
dt = t(2)-t(1);
x1 = X(:,1);
Z = zeros( length(phi), n_bins );
Z(:,1) = pinv(phi)*x1;
Omega = logm(D)/dt;

for k = 1:n_bins
    Z(:,k) = phi*expm(Omega*t(k))*Z(:,1);
end

imagesc(abs(Z))

%% http://www.wikiwaves.org/Numerical_Solution_of_the_KdV

close all
clc

while 1
M = 256;
dt = 1/M;
a = 30;
b = 0;
tau = -0.27;%rand*2-1;
tau2 = 0.24;% tau + rand*(1 - tau);
lambda1 = @(t) b + (t >= tau & t < tau2).*(t - tau)*a + (t >= tau2).*(tau2 - tau)*a;
lambda2 = @(t) b + (t >= tau).*(t - tau)*a;
t = linspace( -1, 1, M );
s = lambda1(t) + randn(size(t));
s = filtfilt( ones(1, 5)/5,1, s);

psi = linspace( 0.01, 0.01, 1 );
for jj = 1:length(psi)

% x = linspace(-1,1,N);
x = s;
t(2) - t(1)
delta_x = abs(x(2) - x(1))
delta_k = 2*pi/(M*delta_x)
k = [0:delta_k:M/2*delta_k,-(M/2-1)*delta_k:delta_k:-delta_k];

f = exp(-x.^2);
f_hat = fft(f);
u = real(ifft(f_hat.*exp(i*k.^3*psi(jj))));
subplot 121
plot(t, s)
subplot 122
plot(t, u)
hold on; plot( [tau tau], [-1 2], 'r--' ); hold off
axis([-1 1 -1 2])
title( sprintf( 'time = %.2f', psi(jj) ) )
drawnow
end

pause
end

%% Intervals
close all

n = 50;
taus = [0,cumsum( rand(1,n) )];
taus = taus'/taus(end)
a = [0, (randn(1,n-1))*10];
b = 0;
t = linspace( 0, 1, 100 );
tw = t(ones(n,1),:);
ind = tw < taus(2:n+1,ones(1,length(t))) & tw >= taus(1:n,ones(1,length(t)));

f = zeros(1,length(t));

for i = 1:length(t) 
    idx = find(t(i) > taus, 1, 'last')
    f(i) = a(idx)*(t(i) - taus(idx)) + sum(a(1:idx-1)'.*(taus(2:idx)-taus(1:idx-1))) + b; 
end


subplot 121
plot( [taus, taus]', repmat( [min(f) max(f)], n+1, 1 )', 'r', 'linewidth', 0.001)
hold on
plot( t, f, 'linewidth', 2 )

subplot 122
histogram( a, 20 )

%% Test of likelihood

close all
clc
N = 50;
n_trials = 10;
n_bins = 100;
interval = [-1, 0];
[data, sparams] = simdata( N, n_trials , 'ramp' );

for i = 2:N
    [r, params] = psth( data{i}, 'Interval', interval, 'NBins', n_bins );
    [rp, params2] = psth( data{i-1}, 'Interval', interval, 'NBins', n_bins );
    t = params.Time;
    if sparams.slope(i) > 0        
        lambda = @(t) (t > sparams.tau(i)).*(t - sparams.tau(i))*sparams.slope(i) + 5;
    else
        lambda = @(t) (t > sparams.tau(i)).*(t - sparams.tau(i))*sparams.slope(i) + 40;
    end
    
    dt = t(2)-t(1);
    edges = [ t - dt/2, t(end) + dt/2 ];
    irate = zeros( 1, length(r) );
    
    for k = 1:length(r)
        M = 100;
        ds = (edges(k+1) - edges(k))/M;
        s = linspace( edges(k), edges(k+1), M );
        irate(k) = sum( lambda(s) )*ds;
    end
    
    likelihood = @(r) sum( r.*log( irate ) - irate );
    llo = likelihood(r);
    ll = zeros( 1, 200 );
       
    for k = 1:length(ll)
        ll(k) = likelihood( r(randperm(length(r))) )
    end
    
    ll2 = likelihood( rp );
    
    figure(2)    
    histogram( ll, 40, 'Normalization', 'probability' )
    hold on
    plot( [llo, llo],[0,1], 'r--' )
    plot( [ll2, ll2],[0,1], 'b--' )
    hold off
    
    figure(1)    
    subplot 121
    plot( t, r, 'k' )
    hold on
    plot( t, rp, 'b' )
    plot( t, lambda(t), 'r--' )     
    hold off
    subplot 122
    plot( t, irate, 'b' );
    hold on
    plot( t, lambda(t), 'r') 
    hold off
    pause
end

%% Piecewise fittitng

close all
fp = @(t) max(0, t);
L = @(t, alpha, tau) alpha'*fp( t - tau );

% Theoretical
t = linspace( 0, 1, 100 );
cp = [0, 0.2 0.4 0.8]';
alpha = [0, 0.5, -0.5, -0.5]';

lambda = L( t, alpha, cp )*10;
size(lambda)
s = lambda + randn( size( lambda ) )*0.1;

subplot 121
plot( t, lambda )
hold on
plot( t, s, 'r' )
hold off

% Estimation

theta0 = rand( 2*length( cp ) - 1, 1 );
% theta0 = [alpha;tau(2:end)];
theta = fitpw( s, t, theta0 );

n = length(theta);
I = ceil(n/2);
alpha = theta( 1:I );

tau = [0;theta( I+1:end )];

subplot 122
plot( t, s );
hold on
plot( t, L( t, alpha, tau ), 'r' )
hold off

%% Recursive fitting


close all
fp = @(t) max(0, t);
L = @(t, alpha, tau) alpha'*fp( t - tau );

% Theoretical
t = linspace( 0, 1, 100 );
cp = [0, rand(1,2)]';
alpha = [rand(size(cp)) - .5];

lambda = L( t, alpha, cp )*20;
so = lambda + randn( size( lambda ) )*0.1;
[s, params] = bilateralfilt( so, 'Time', t );
[s, params] = simplify( s, 'Time', t );
ss = params.Simplification(:,2);
tt = params.Simplification(:, 1);
X = [ones(size(s))', t' ];
Y = s';
B = X\Y;

% Finding the distance
d = @(p) abs( p(2) - B(2)*p(1) - B(1) )/sqrt(1 + B(2)^2);
sd = arrayfun( @(i) d([t(i), s(i)]), 1:length(t) ) ;
sd2 = arrayfun( @(i) d([tt(i), ss(i)]), 1:length(tt) ) ;
[~, I] = max(sd );

figure
plot( t, so, 'color', [0.7 0.7 0.7] )
hold on
plot( t, s, 'b' )
plot( t, B(1) + B(2)*t, 'r' )
% plot( [t(1) t(end)], [s(1), s(end)], 'k' )
xp = t(I);
yp = s(I);
abs( yp - B(2)*xp - B(1) )/sqrt(1 + B(2)^2)
plot( xp, yp, 'r*' )
plot( tt, ss, 'b--')
plot( [cp, cp]', repmat([min(so); max(so)], 1, length(cp)), 'r--')
[pks, locs] = findpeaks( sd );
locs = locs( pks > 0.01*mean(abs(s)) );
plot( [t(locs); t(locs)], repmat([min(so); max(so)], 1, length(locs)), 'k--' )
hold off

figure
plot( t, sd )
hold on
bar( tt,sd2, 'r' )
hold off

%%









