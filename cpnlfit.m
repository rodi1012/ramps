function [y, result] = cpnlfit( y, varargin )

t = getArg( varargin, 'Time', 1:length(y) );
type = getArg( varargin, 'FitType', 'pwlinear');
interval = getArg( varargin, 'Interval', [-1,0] );
draw = getArg( varargin, 'Draw', true );
tau0 = getArg( varargin, 'Tau', [] );
slope0 = getArg( varargin, 'Slope', [] );

dt = t(2)-t(1);

switch type
    case 'pwlinear'
        P0 = getArg( varargin, 'P0', rand(1,3) );
        L = @(slope, tau, base, x) (x > tau).*(slope*(x-tau)) + base;
        ft = fittype(L);
        fo = fitoptions('Method', 'NonlinearLeastSquares', ...
                'Algorithm', 'Levenberg-Marquardt',...
                'Upper', [Inf, interval(2), Inf], ...
                'Lower', [-Inf, interval(1), -Inf], ...
                'StartPoint', P0, ...
                'MaxFunEvals', 600, ...
                'MaxIter', 1000);

    case 'sigmoid'
        P0 = getArg( varargin, 'P0', rand(1,4) );
        P0(2) = tau0;
        P0(1) = slope0;
        L = @(slope, tau, tau2, base, x) base + ...
                       (x > tau & x <= (tau2+3*dt)).*(slope*(x - tau)) + ...
                       (x > (tau2+3*dt)).*(slope*((tau2+3*dt) - tau));
        ft = fittype(L);
        fo = fitoptions('Method', 'NonlinearLeastSquares', ...
                'Algorithm', 'Levenberg-Marquardt',...
                'Upper', [Inf, interval(2), interval(2), Inf], ...
                'Lower', [-Inf, interval(1), interval(1), -Inf], ...
                'StartPoint', P0, ...
                'MaxFunEvals', 600, ...
                'MaxIter', 1000);
end

[yfit, G] = fit(t',y', ft, fo );
result = struct;
result.tau = yfit.tau;
result.slope = yfit.slope;
result.tau2 = t(end);
result.B0 = yfit.base;
result.R2 = G.rsquare;

if strcmp( type, 'sigmoid' )
    result.tau2 = yfit.tau2; 
end

if draw
    clf
    subplot 111
    switch type
        case 'pwlinear'
            g = @(t) L(result.slope, result.tau, yfit.base, t );        
        case 'sigmoid'        
            g = @(t) L(result.slope, result.tau, result.tau2, yfit.base, t );
    end

    plot( t, y )
    hold on
    plot( t, g(t), 'r' )
    axis( [-1 0 -max(abs(y)) max(abs(y))] )
    hold off
    pause
end

    
end