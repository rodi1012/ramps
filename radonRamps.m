function [beta0, beta1, tau, accept] = radonRamps( y, t )
% 

M = length(t);
theta = linspace( -pi/2, pi/2, 100 );
rho = linspace( 0,1,100 );
actualRhos = zeros(M,length(theta) );

A = zeros( length(rho), length(theta) );
pxs = [];
pys = [];

for i = 1:M
    v = [t(i), y(i)]';
    for j = 1:length(theta)
        a = [cos(theta(j)), sin(theta(j))]';
        p = v'*a;
        
        actualRhos(i,j) = p;
        [~,Irho] = sort( abs(rho - p), 'ascend' );
        Irho = Irho(1);
        
        A( Irho, j ) = A( Irho, j ) + 1;
    end
    
end

[I,J] = find(A > 10);

pxs = rho(I);
pys = theta(J);

size(pxs)
plot( pxs, pys, '*' )
axis( [min(rho) max(rho) min(theta) max(theta)] )
pause
% figure(1)
% hist(actualRhos(:), 100 )
% 
% figure(2)
% subplot 121
% 
% imagesc(theta, rho, A>30)
% xlabel('Angle')
% ylabel('p')
% colormap bone
% 
% 
% [~,midx] = sort( A(:), 'descend');
% [I,J] = ind2sub( size(A), midx(1:10) );
% 
% mtheta = theta( J );
% mrho = rho( I );
% 
% subplot 122
% plot( t, y )
% hold on
% 
% for i = 1:length(mtheta)
%     x = linspace( 0, 1, 100 );
%     y = (mrho(i) - x*cos(mtheta(i)))/sin(mtheta(i));
%     plot(x,y,'color', [.8 .8 .8], 'linewidth', .5)
% end
% 
% hold off

