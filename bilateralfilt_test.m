function [r, params] = bilateralfilt_test( r, varargin )

draw = getArg( varargin, 'Draw', 0 );
s = getArg( varargin, 'Sigma', 0.05 );
t = getArg( varargin, 'Time', 1:length(r) );
p = 2;
vals = linspace( 2, 2, 1);

for j = 1:length( vals) 
    s_int = vals(j);   
    s = 0.05;
    lambda = 0.1;
    gamma_e = 20;
    gamma_d = vals(j);
    beta = 5;
    G = @(x) exp( -x.^2/(2*s_int^2) )/(sqrt(2*pi)*s_int );  
    H = @(x) gamma_e*(x.^p)./(gamma_d + gamma_e*(x.^p));
    W = @(x) exp( -x.^2/(2*s^2) )/(sqrt(2*pi)*s );  
%     W = @(x) exp(-lambda*abs(x))/(2*lambda);
%     f = @(u) 1./(1 + exp(-beta*u));
    f = @(u) u;
    r_filt = zeros( size(r) );

    for i = 1:length(r)
        K = sum( G(abs(r - r(i))).*W(abs(t - t(i))));
        r_filt(i) = sum(f(r).*G(abs(r - r(i))).*W(abs(t - t(i))))/K;
%         K = sum( H(r*r(i)).*W(abs(t - t(i))));
%         K = (K<=0)*0.001 + K;
%         r_filt(i) = sum(f(r).*H(r*r(i)).*W(abs(t - t(i))));
    end

    if draw
        subplot 121
        plot( t, r )
        subplot 122
        plot( t, r_filt, 'r' )        
        pause
    end
end
r = r_filt;
params = struct;
