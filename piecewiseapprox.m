%% Piecewise curves
close all
t = linspace( 0, 1, 100 );
cp = rand( 1, 3 );

fp = @(t) max( 0, t );
w = @(x) -((x - cp(1))*(x - cp(2))*cp(3) + (x - cp(3))*(x - cp(1))*cp(2) + (x - cp(2))*(x - cp(3))*cp(1));
B = @(t) (cp(3) - cp(1))*sum(arrayfun(@(k)fp(t - cp(k))./w(cp(k)), 1:length(cp)));
r = zeros(1, length(t));

for i = 1:length(t)    
    r(i) = B(t(i));
end

plot( t, r )
hold on
plot( [cp(1) cp(1)], [min(r) max(r)], 'r--' )
plot( [cp(2) cp(2)], [min(r) max(r)], 'r--' )
plot( [cp(3) cp(3)], [min(r) max(r)], 'r--' )
hold off

%% Lagrange

close all
t = linspace( 0, 1, 100 );
cp = [0, rand( 1, 1 ), 1];
f = [0, 0, 1];
L = zeros(size(t));

for k = 1:length(t)
    for i = 1:length(cp)-1 
        if t(k) >=cp(i) && t(k) <= cp(i+1)
            h = cp(i+1)-cp(i);
            L(k) = f(i)*(cp(i+1) - t(k))/h + f(i+1)*(t(k) - cp(i))/h;
        end
    end
end

plot(t, L )

%% PW linear

close all
clc

t = linspace( 0, 1, 100 );
fp = @(t) max(0, t);
L = @(t, alpha, tau) alpha'*fp(t - tau);
tau = [0, rand( 1, 2 )]';
alpha = [0, 0.5, -0.5]';


for i = 1:length(t)
    lambda(i) = L(t(i), alpha, tau )*40;
end

s = lambda + randn(size(lambda))*0.1;
subplot 121
plot(t, s)
hold on
plot(t, lambda, 'r' )
hold off

theta = fitpw( s, t, [alpha', tau(2:end)']');
n = length(theta);
idx = ceil(n/2);
tau_e = [0; theta(idx+1:end)];
alpha_e = theta(1:idx);

subplot 122
plot(t, s)
hold on
plot(t, L(t, alpha_e, tau_e), 'r' )
hold off

%% Likelihood functions again
close all
clc
n = 3;
taus = [0, 0.3 0.6 1];
alpha = rand(1,n);

n = 2
tausx = [0, 0.3 1];
alphax = rand(1,n);

if all(locs ~= m)
    locs = [locs, m];
end
a = 0;
b = 1;
N = 100;
t = linspace(a, b, N);
dt = (b - a)/N;

[r, lambda, ilambda] = genproc( t, alpha, taus );
[rx, lambdax, ilambdax] = genproc( t, alphax, tausx );

lili = @(ilambda) sum( r.*log(ilambda) - ilambda - log(factorial(r)) );

subplot 121
plot( (t(1:end-1)+t(2:end))/2, lambda )
hold on
plot( (t(1:end-1)+t(2:end))/2, r, 'k' )
plot( [taus;taus], repmat([min(lambda);max(lambda)], 1, length(taus)), 'r--' )
hold off


subplot 122
plot( (t(1:end-1)+t(2:end))/2, lambda )
hold on
plot( (t(1:end-1)+t(2:end))/2, rx, 'color', [0.7 0.7 0.7] )
plot( (t(1:end-1)+t(2:end))/2, lambdax, 'r--' )
plot( [taus;taus], repmat([min(lambda);max(lambda)], 1, length(taus)), 'r--' )
hold off

figure
bar([lili(ilambda) lili(ilambdax)] );
