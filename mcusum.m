function [tau, slope] = mcusum( y, t, varargin )
% mcusum    Applies matlab's cusum algorithm to the detection of the change
% point

cp = cusum( y );

if isempty(cp)
    cp = length(t);
end

cp = cp( find( cp(:,1) > 0, 1, 'first' ), 1 );
[tau, slope] = refine( y, t, cp, varargin{:} );    

end