function [r1,r2,L,t]=ks(Data,R,Crit)
% KS computes the logit vector using the Kolmogorov-Smirnov test for
% whether two distributions differ
% The syntax is [r1,r2,L,t]=ks(Data,R,Crit)
% Data is a column vector of successive measures (interevent intervals,
% responses, poke durations, etc). NB, it is not the cumulative vector!
% R is the vector of putative change points.
% Crit is the decision criterion.
% All arguments are obligatory.
% L is the pseudologit vector for rows where the
% approximation formula for the Kolmogorov-Smirnov p is valid.
% Its final value is the first value to exceed the decision criterion
% t is the col vector of rows for which L is defined
% r1 is the change point row when the decision criterion is exceeded
% r2 is the row at which the decision criterion is exceeded
% If there are no testable rows or if the decision criterion is never
% exceeded, the variables are returned empty

N=(1:length(Data))'; % Number of rows in Data vector

r1=[];r2=[];L=zeros(length(N),1); t=[]; % Initializing

Na=N-R; % Col vector giving for each row in Data the number of rows
%           after the putative change point. So R gives the number of
%           rows before the change point and Na the number after

Test=find((Na.*R)./(Na+R)>=4); % For the approximation to the KS
%           probability to be valid, the product of the two n's divided by
%           the sum must be greater than or equal to 4. Test is the col
%           vector of rows that satisfy this constraint--the row numbers(!),
%           not the entries themselves

if isempty(Test) % There are no rows satisfying the constraint
    
   return % Bail if there are no testable points
   
end;

for T =1:length(Test) % Loop that steps through Data performing the KS test wherever
    %   it is valid, until the resulting logit exceeds the decision
    %   criterion
    
    [H(T),P(T)]=kstest2(Data(1:R(Test(T))),Data(R(Test(T))+1:Test(T)));
    
    L(T)=abs(log10((1-P(T))/P(T)));
    
    t(T)=Test(T); % The row to which the latest value of L(T) "belongs"
    
    if L(T)>Crit % Value of logit exceeds decision criterion
        
        r2=Test(T); % the row (in Data) at which the criterion is exceeded

        r1=R(Test(T)); % the change point  when the criterion is exceeded,
         
        L(T+1:end)=[]; % Drop rows of L after row in which decision criterion reached

        break % Break out of loop when decision criterion exceeded
        
    end % of if
    
end % of for

t=t'; % Makes t a column vector, like L