function [r,params] = wvfilt( r, varargin )
% WVFILT    Extracts the trend using wavelets

r = getArg( varargin, 'OriginalRate', r );
draw = getArg( varargin, 'Draw', false );
t = getArg( varargin, 'Time', 1:length(r) );
L = 3;
wv = modwt( r, 'haar', L, 'reflection' );
mra = modwtmra( wv, 'haar');

if draw
    plot( t, r )
    hold on
    plot( t, mra(end,1:length(t)), 'r')
    plot( t, wv(end,1:length(t)), 'r--')
    hold off
    title('Different wavelets (press a key to continue)')
    pause
end

params = struct( 'OriginalRate', r );
r = mra( end, 1:length(t) );
