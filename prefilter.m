function [Xrate, idxs] = prefilter( Xrate, t, varargin )
% Preselects some neurons based on PCA coefficients

type = getArg( varargin, 'Type', 'pca');
posi = [];
negi = [];

switch type
    case 'pca'
        [posi, negi, pc, nc] = preselect( Xrate ) ;
%         subplot 121
%         imagesc(Xrate(posi,:), [0,1])
%         subplot 122
%         imagesc(Xrate(negi,:), [0,1])
%         pause
end
% Returning 
idxs = [posi; negi];