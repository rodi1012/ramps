function respool = analysisRamps( data, varargin ) 

% Options
map = getArg( varargin, 'Colormap', hot );
figures = getArg( varargin, 'Figures', [] );
rate = getArg( varargin, 'Rate', [] );
detection = getArg( varargin, 'Detection', [] );
n_bins = getArg( varargin, 'NBins', 100 );
interval = getArg( varargin, 'Interval', [-1, 0] );
dzscore = getArg( varargin, 'ZScore', false );
dsmooth = getArg( varargin, 'Smooth', false );
dpreselect = getArg( varargin, 'PreSelect', true );

regions = fieldnames(data);

for i = 1:length(regions)
    fprintf('Performing region %s\n', regions{i}) 
    Xspikes = data.(regions{i});    
    respool = doAnalysis( Xspikes, varargin{:} );
    
    I = find(respool.ps > 0.1);
%     I = 1:length(respool.ps);
   
    
end