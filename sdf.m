function [r, t] = sdf( x, kernel, nbins, I, sigma )
% Computes the sdf from the data
% x is a cellarray
% K is the kernel
% I is the interval

switch kernel
    case 'step'
        K = @(t) ( abs(t) <= sigma/2 )*(1/sigma);    
    case 'gaussian'    
        K = @(t) exp( -t.^2/(2*sigma^2) )/(sqrt(2*pi)*sigma );         
    case 'asymmetric'
        td = sigma*.2;
        tg = sigma*.8;
        A = (td^2)/(td  + tg);
        K = @(t) (t >= 0 ).*(1 - exp(-t/tg)).*exp(-t/td)/A;
    otherwise
        error('Kernel not supported')
end

n = max(x(:,1));
t = linspace( I(1), I(2), nbins );
r = zeros( 1, nbins );

for i = 1:size(x,1)
    r = r + K(t - x(i,2))/n;
end

