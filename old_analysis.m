%% General test
addpath( 'bars' )
close all
N = 10;
n_trials = 100;
n_bins = 100;
interval = [-1, 0];
[data, params] = simdata( N, n_trials , 'ramp' );
neurons = zeros( N, n_bins );
sneurons = zeros( N, n_bins );


for i = 1:N    
    x = data{i};
    [neurons( i, : ), t] = psth( x, n_bins, interval, true );
    subplot 121
%     raster( data{i} );
    res = barsP( neurons( i, : ), [ interval(1), interval(end)], n_trials );
    plot(t, neurons( i, : ))
    hold on
    plot(t, res.mean )
    plot( [params.tau(i), params.tau(i)], [0,1], 'r', 'linewidth',2  )
    hold off
    subplot 122
    
    sigma = .05;
    [r,t] = sdf( x, 'step', n_bins, interval, sigma );
    plot( t, r, 'b' )
    hold on
    [r,t] = sdf( x, 'gaussian', n_bins, interval, sigma );
    plot( t, r, 'g' )
    sigma = .1;
    [r,t] = sdf( x, 'asymmetric', n_bins, interval, sigma );
    plot( t, r, 'c' )
        
    sneurons( i, : ) = r;
    
    [r,t] = traditionalRate( x, .05, .02, interval );
    plot( t, smooth( r, 5 ) , 'k' )
%     res = barsP( neurons( i, : ), [ interval(1), interval(end)], ntrials );
%     sneurons( i, : ) = res.mean; 
    
    
    plot( [params.tau(i), params.tau(i)], [0,1], 'r', 'linewidth',2  )
    hold off
    pause
end

figure
imagesc( sneurons )

%% Square error distribution per methods for different taus and slopes

addpath( 'bars' )
addpath( '../' )
close all
clear all
clc
% Parameters
N = 100;
n_trials = 100;
n_bins = 200;
interval = [-1, 0];
sigma = .05;

% Data
[data, params] = simdata( N, n_trials , 'ramp' );
neurons = zeros( N, n_bins );
exmean = @(f)f.mean';
ratefns = struct( 'psth', @(x) psth( x, n_bins, interval, false ), ...
                  'bars', @(x) exmean(barsP( psth( x, n_bins, interval, false ), ...
                                                  [ interval(1), interval(end)], n_trials )), ...
                  'sdfstep', @(x)sdf( x, 'step', n_bins, interval, sigma ), ...
                  'sdfasym', @(x)sdf( x, 'asymmetric', n_bins, interval, sigma ));
fns = struct( 'gallistel', @gallistel, ...
              'mcumsum', @mcusum, ...
              'nlfit', @cpnlfit, ...
              'ratechange', @(r,t)rchange(r,t, 'method', 'confidence'), ...
              'radon', @cpradon, ...
              'derivatives', @(r,t) cpdiff(r,t, 'psth', r_psth) );          
taus = struct;
slopes = struct;
e_tau = struct;
e_slope = struct;

% Distribution errors tau
figure(1)
% Distribution errors slope
figure(2)
% Relation tau
figure(3)
% Relation slope
figure(4)

fnames = fields( fns );
ratenames = fields( ratefns );

for j = 1:length(ratenames)
    ratefn = ratefns.(ratenames{j});
    fprintf('Performing analysis for rate fn %s\n', ratenames{j})
    % Initialization
    for k = 1:length(fnames)
        taus.(fnames{k}) = [];
        slopes.(fnames{k}) = [];
        e_tau.(fnames{k}) = [];
        e_slope.(fnames{k}) = [];
    end

    % Processing
    for i = 1:N    
        x = data{i};        
        r = ratefn(x);          
        neurons( i, : ) = r;         

        for k = 1:length(fnames)
            fn = fns.(fnames{k});
            [tau, slope] = fn( r, t );
            taus.(fnames{k}) = [taus.(fnames{k}), tau];
            slopes.(fnames{k}) = [slopes.(fnames{k}), slope];
            e_tau.(fnames{k}) = [e_tau.(fnames{k}), (tau - params.tau(i))^2];
            e_slope.(fnames{k}) = [e_slope.(fnames{k}), ((slope - params.slope(i))^2)/params.slope(i)];    
        end
    end
    
    fprintf('Plotting!\n')
    figure(1)
    
    for k = 1:length(fnames)
        subplot(length(ratenames), length(fnames), length(fnames)*(j-1)+k )    
        histogram(e_tau.(fnames{k}), linspace(0,2,40));    
        title(fnames{k})
        xlabel('error')
    end

    figure(2)
    for k = 1:length(fnames)
        subplot(length(ratenames), length(fnames), length(fnames)*(j-1)+k )    
        histogram(e_slope.(fnames{k}),40);    
        title(fnames{k})
        xlabel('error')
    end
    
    figure(3)
    for k = 1:length(fnames)
        subplot(length(ratenames), length(fnames), length(fnames)*(j-1)+k )  
        plot(params.tau, e_tau.(fnames{k}), '.');    
        axis([-1 0 0 2])
        title(fnames{k})
        xlabel('tau')
        ylabel('error')
    end
    
    
    figure(4)
    for k = 1:length(fnames)
        subplot(length(ratenames), length(fnames), length(fnames)*(j-1)+k )  
        plot(params.slope, e_slope.(fnames{k}), '.');    
%         axis([-1 0 0 2])
        title(fnames{k})
        xlabel('slope')
        ylabel('error')
    end
end

%% Analysis same point

addpath( 'bars' )
addpath( '../' )
close all
clear all
clc
% Parameters
N = 100;
n_trials = 100;
n_bins = 200;
interval = [-1, 0];
sigma = .05;

% Data
[data, params] = simdata( N, n_trials , 'ramp', 'FixRates', true );

neurons = zeros( N, n_bins );

exmean = @(f)f.mean';

ratefns = struct( 'psth', @(x) psth( x, n_bins, interval, false ), ...
                  'bars', @(x) exmean(barsP( psth( x, n_bins, interval, false ), ...
                                                  [ interval(1), interval(end)], n_trials )), ...
                  'sdfstep', @(x)sdf( x, 'step', n_bins, interval, sigma ), ...
                  'sdfasym', @(x)sdf( x, 'asymmetric', n_bins, interval, sigma ));

fns = struct( 'gallistel', @(r,t)gallistel(r,t,'lines', true), ...
              'mcumsum', @mcusum, ...
              'nlfit', @cpnlfit, ...
              'ratechange', @(r,t)rchange(r,t, 'method', 'confidence'), ...
              'radon', @cpradon, ...
              'derivatives', @(r,t) cpdiff(r,t) );
          
taus = struct;
slopes = struct;
e_tau = struct;
e_slope = struct;

% Distribution tau
figure(1)
% Distribution slopeActual 
figure(2)
% Actual value slope
figure(3)

fnames = fields( fns );
ratenames = fields( ratefns );params.tau(i)
        params.slope(i)
        plot(t,r_psth)
        pause

for j = 1:length(ratenames)
    ratefn = ratefns.(ratenames{j});
    fprintf('Performing analysis for rate fn %s\n', ratenames{j})
    % Initialization
    for k = 1:length(fnames)
        taus.(fnames{k}) = [];
        slopes.(fnames{k}) = [];
        e_tau.(fnames{k}) = [];
        e_slope.(fnames{k}) = [];
    end

    % Processing
    for i = 1:N    
        x = data{i};  
        [r_psth, t] = psth( x, n_bins, interval, false );         
        r = ratefn(x);          
        neurons( i, : ) = r;         
        
%         figure(5)      
        for k = 1:length(fnames)
            fn = fns.(fnames{k});
            [tau, slope] = fn( r, t );            
%             hold on            
%             kc = find( t >= tau, 1, 'first');
%             plot( t, slope*(t - tau) + r_psth(kc), 'color', [.5 .5 .5])
%             plot( t(kc), r_psth(kc), 'r*' )
%             
            taus.(fnames{k}) = [taus.(fnames{k}), tau];
            slopes.(fnames{k}) = [slopes.(fnames{k}), slope];
            e_tau.(fnames{k}) = [e_tau.(fnames{k}), (tau - params.tau(i))^2];
            e_slope.(fnames{k}) = [e_slope.(fnames{k}), ((slope - params.slope(i))^2)/params.slope(i)];    
        end        
    end
    
%     plot( t, r_psth, 'b', 'linewidth', 2)        
%     pause
    fprintf('Plotting!\n')
    figure(1)
    
    for k = 1:length(fnames)
        subplot(length(ratenames), length(fnames), length(fnames)*(j-1)+k )    
        histogram(taus.(fnames{k})-params.tau(1), linspace(-1,1,40));    
        title(fnames{k})
        xlabel('error')
    end

    figure(2)
    for k = 1:length(fnames)
        subplot(length(ratenames), length(fnames), length(fnames)*(j-1)+k )    
        histogram((slopes.(fnames{k})-params.slope(1))/params.slope(1),40);    
        title(fnames{k})
        xlabel('error')
    end
    
    figure(3)
    for k = 1:length(fnames)
        subplot(length(ratenames), length(fnames), length(fnames)*(j-1)+k )    
        histogram(slopes.(fnames{k}),40);    
        title(fnames{k})
        xlabel(sprintf('Slope (actual: %f', params.slope(1)))
    end
   
end

%% Confidence intervals and significance

addpath( '../' )
close all
clc
% Parameters
N = 1;
n_trials = 100;
n_bins = 200;
interval = [-1, 0];
sigma = .05;

% Data
fprintf('Generating data...\n')
[data, params] = simdata( N, n_trials, 'ramp', 'FixRates', true, 'tau', -0.2, 'slope', 200 );
[r1, t] = psth( data{1}, n_bins, interval, false ); 
[data, params] = simdata( N, n_trials, 'ramp', 'FixRates', true, 'tau', 0, 'slope', 0 );
[r2, t] = psth( data{1}, n_bins, interval, false ); 
% Rate one
fprintf('Performing the fitting for the first rate...\n')
[tau, slope] = cpnlfit( r1, t );
fprintf('Bootstrap confidence intervals...\n')
[L, U] = rbootstrap( r1, t, @cpnlfit, tau, 'interval', interval, 'method', 'percentil', 'slope', slope );
fprintf('Computing significance...\n')
p = rsignificance( r1, t, tau );
subplot 121
plot( t, r1 )
hold on
M = max(abs(r1));
plot( [tau, tau], [-M, M], 'r' )
plot( [L, L], [-M, M], 'r--' )
plot( [U, U], [-M, M], 'r--' )
title(sprintf('Example trend (p = %.2f)', p))
hold off

% Rate two
fprintf('Performing the fitting for the second rate...\n')
[tau, slope] = cpnlfit( r2, t );
fprintf('Bootstrap confidence intervals...\n')
[L, U] = rbootstrap( r2, t, @cpnlfit, tau, 'interval', interval, 'method', 'percentil', 'slope', slope );
fprintf('Computing significance...\n')
p = rsignificance( r2, t, tau );
subplot 122
plot( t, r2 )
hold on
M = max(abs(r2));
plot( [tau, tau], [-M, M], 'r' )
plot( [L, L], [-M, M], 'r--' )
plot( [U, U], [-M, M], 'r--' )
title(sprintf('Example no trend (p = %.2f)', p))
hold off

%% Page cusum
close all
N = 100;
n_trials = 100;
n_bins = 100;
interval = [-1, 0];    mu = mean( r );
    sigma = std( r );    
    r = (r - mu)/sigma;

[data, params] = simdata( N, n_trials , 'sigmoid' );

dt = 1/n_bins;

for i = 1:N
    trials = data{i};
    [r,t] = psth( trials, n_bins, interval, false );
    mu = mean( r );
    sigma = std( r );    
    r = (r - mu)/sigma;
    theta = linspace( 40, 5, 50 );
    delta = abs(mean(r((end-10):end)));
    for k = 1:length(theta)       
        [tau, slope] = pagecusum( r, t, 'distribution', 'normal', 'delta', delta, 'threshold',theta(k) );
        taus(k) = tau;
    end
    
    [pks, locs] = findpeaks( abs(diff(taus)) );
    epks = [1, locs', length(theta)];
    [~,I] = max( abs(diff( epks )) );
    idx = fix((epks(I) + epks(I+1))/2);
    [tau, slope] = pagecusum( r, t, 'distribution', 'normal', 'delta', delta, 'threshold', theta(idx) );
    
    subplot 311
    hold on
    plot( params.tau(i), 0, 'r*' )
    hold off
    
    figure(2)
    subplot 211
    plot( diff(taus)  )
    subplot 212
    plot( taus )
    figure(1)
    pause
    
end

%% Fitting nonlinear - piecewise
close all
N = 100;
n_trials = 100;
n_bins = 100;
interval = [-1, 0];
[data, params] = simdata( N, n_trials , 'sigmoid' );

for i = 1:N
    clc
    trials = data{i};
    [r,t] = psth( trials, n_bins, interval, false );
    [tau, slope, G, I] = cpnlfit( r, t, 'type', 'sigmoid', 'Interval', interval );
    
    hold on
    plot( params.tau(i), 0, 'r*' )
    hold off
    pause
end